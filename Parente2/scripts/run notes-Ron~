filename=haplotypes1

# First, link all of the stuff in ../phasing/phased-without-missing into the ./data directory
cd data
ln -s ../../phasing/phased-without-missing/* .
cd ..

# Here we will define adjacent windows of size 5 (basically just
# uses the markers to do this, not any haplotype frequency info)
for chr in {1..22}; do
  script/make_fwins.rb data/$filename$chr.tped adj 5 > model/$chr.fwin
done

# Now we will compute the haplotype frequencies per window as 
# defined by the window file we just created.
# the output format is: <window-subset-id>	<haplotype>	<haplotypes-counted-in-data>
for chr in {1..22}; do
  script/win-haplo-map.py model/$chr.fwin data/$filename$chr.tped > model/$chr.hf
done


# And now the magic finally gets to happen:
mkdir train
# Train
for chr in {1..22}; do
  ../parente/bin/parente train\
    --threads 40 \
    --wset-size 5 \
    --lrt2-type hist\
    --lrt2-args p=0.01,b=30 \
    --TH data/$filename$chr.hap\
    --num-pairs 100\
    --map data/$filename$chr.tped\
    model/$chr.hf \
    model/$chr.fwin \
    train/train
done

thresh_min=-225
cm=4
max_cm="$(echo $cm - 0.1 | bc -l)"
cm_low_offset="0$(echo "$cm * 0.1" | bc -l)"
mkdir infer

# Infer
for chr in {1..22}; do
  ../parente/bin/parente infer\
  --threads 40 \
  --min-block-size-delta $cm_low_offset\
  --lrt 2\
  --lrt2-type hist \
  train/train \
  data/$filename$chr.geno \
  $max_cm \
  $thresh_min \
  infer/$chr
done
