from __future__ import division
import sys

def concat(in_prefix,in_postfix, out_file):
	for i in range(1,23):
		filename = in_prefix+str(i)+in_postfix
		f = open(filename)
		first_line = True
		for line in f:
			if first_line:
				first_line = False
				if i != 1:
					continue
			out_file.write(line)
filename_prefix = sys.argv[1] #including full path
final_hap = open(filename_prefix+".hap","w")
final_geno = open(filename_prefix+".geno","w")
concat(filename_prefix,".hap",final_hap)
concat(filename_prefix,".geno",final_geno)
