#!/usr/bin/env ruby

if ARGV.size < 2
  puts "args: <ph-prefix> <out_prefix>"
  exit 1
end

in_prefix = ARGV.shift
out_prefix = ARGV.shift

fin_phgeno = File.new(in_prefix + '.phgeno')
fin_phind = File.new(in_prefix + '.phind')
fin_phsnp = File.new(in_prefix + '.phsnp')

fout_tped = File.new(out_prefix + ".tped", "w")
fout_tfam = File.new(out_prefix + ".tfam", "w")

snps = []
haps = haps = []

for line in fin_phind
  f = line.chomp.split
  hap = f[0].split(":")[1]
  haps << hap
end

# Write tfam
i = 0
while i < haps.size
  fout_tfam.puts([0, haps[i].sub(/_A$/, ""), 0, 0, 0, -9].join("\t"))
  i += 2
end

# read SNPs
for line in fin_phsnp
  f = line.chomp.split

  snps << {:rsid => f[0],
           :chrom => f[1],
           :gpos => f[2],
           :ppos => f[3],
           :a1 => f[4],
           :a0 => f[5]}
end

# read phgeno
# write tped
marker = 0
for line in fin_phgeno
  line.chomp!
  snp = snps[marker]
  fout_tped.print([
      snp[:chrom],
      snp[:rsid],
      snp[:gpos],
      snp[:ppos],
  ].join("\t"))

  for hap in 0...line.size
  
    if line[hap] == '0'
      fout_tped.print("\t" + snp[:a0])
    else
      fout_tped.print("\t" + snp[:a1])
    end
  end
  marker += 1
  fout_tped.puts
end


# .phsnp and .phind
foo = <<-EOS
           rs4245756   1        0.002622989938          799463 T C
           rs4075116   1        0.561657011509         1003629 C T
           rs9442385   1        0.882629990578         1097335 T G
          rs10907175   1        0.996878027916         1130727 C A
           rs2887286   1        1.083490014076         1156131 C T
           rs6603781   1        1.092020034790         1158631 A G
          rs11260562   1        1.114789962769         1165310 A G
           rs6685064   1        1.278409957886         1211292 T C
            rs307378   1        1.478119969368         1268847 T G
           rs1695824   1        1.816969990730         1365570 A C
     0:WTCCC66157_A   U   Unknown
     0:WTCCC66157_B   U   Unknown
     0:WTCCC66061_A   U   Unknown
     0:WTCCC66061_B   U   Unknown
     0:WTCCC66062_A   U   Unknown
     0:WTCCC66062_B   U   Unknown
     0:WTCCC66063_A   U   Unknown
     0:WTCCC66063_B   U   Unknown
     0:WTCCC66064_A   U   Unknown
     0:WTCCC66064_B   U   Unknown
EOS

fout_tped.flush
fout_tped.close
fout_tfam.flush
fout_tfam.close
