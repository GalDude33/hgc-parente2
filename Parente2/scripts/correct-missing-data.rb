#!/usr/bin/env ruby

require 'bix'

# At sites where there is only 1 allele observed in the frq file,
# will convert all values (including missing or minor alleles) to
# the major allele

if ARGV.size < 2
  puts "args: <in.tped> <in.frq>"
  puts "  Prints new tped to stdout"
  exit 1
end

frq = Bix::read_frq(ARGV[1])
tped = Bix::read_tped(ARGV[0])

# First overwrite all genotypes at monoallelic positions to be the major allele
for m in 0...frq.size
  if frq[m][:a1] == '0' # plink says only 1 allele was ever observed
    maj = frq[m][:a2]
    for person_idx, hap_pairs in tped
      # Overwrite all alleles at this position to be the major allele
      hap_pairs[0][m] = maj
      hap_pairs[1][m] = maj
    end
  end
end


# Now print the newly modified file
m = 0
for line in File.new(ARGV[0])
  print line.chomp.split[0, 4].join("\t") # meta info about each marker
  for person_idx, hap_pairs in tped
    # Overwrite all alleles at this position to be the major allele
    print ["", hap_pairs[0][m], hap_pairs[1][m]].join("\t")
  end
  puts
  
  m += 1
end
