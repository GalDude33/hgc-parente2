from __future__ import division
import sys

acc_cm = 0
snpinfo_filename = sys.argv[1]
map_out_name = sys.argv[2]
with open(snpinfo_filename,"r") as in_snp:
	with open(map_out_name,"w") as out_snp:
		prevloc = 0
		for i,line in enumerate(in_snp):
			if i==0:
				continue
			sl = line.split()
			currloc = int(sl[2])
			cm = 0	
			if prevloc != 0:
				cm = float(currloc-prevloc)/float(1000000)
			acc_cm = acc_cm + cm
			prevloc = currloc
			out_snp.write(sl[1][3:]+" "+sl[0]+" "+'{0:.20f}'.format(acc_cm)+" "+sl[2]+"\n")
