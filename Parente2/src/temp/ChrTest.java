package temp;


import java.io.IOException;

import parente.SnpInfoReader;


public class ChrTest {

	public static void main(String[] args) {		
		String snpInfoFilePath = args[0];
		
		SnpInfoReader snpInfoReader = new SnpInfoReader();
		try {
			snpInfoReader.fillSnpInfo(snpInfoFilePath);
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		int min = Integer.MAX_VALUE;
		int minChr = 0;
		
		for(int chr=1; chr<=22; chr++){
			int chrSize = snpInfoReader.getChromosomeSnps(chr).size();
			if(chrSize < min){
				min = chrSize;
				minChr = chr;
			}
		}
		
		System.out.println(minChr);
		
		for(int chr=1; chr<=22; chr++){
			System.out.println("chr="+chr+"\t size="+snpInfoReader.getChromosomeSnps(chr).size());
		}
	}
}
