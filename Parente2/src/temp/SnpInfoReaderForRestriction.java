package temp;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class SnpInfoReaderForRestriction {

	private Map<Integer, Map<Integer,Integer>> chromosomeSnpsMap;
	
	public SnpInfoReaderForRestriction() {
		this.chromosomeSnpsMap = new HashMap<Integer, Map<Integer,Integer>>();
		
		for(int i=1; i<=22; i++){
			chromosomeSnpsMap.put(i, new HashMap<Integer,Integer>());
		}
	}
	
	public void fillSnpInfo(String filePath) throws NumberFormatException, IOException{
		File snpFile = new File(filePath);
		BufferedReader br = new BufferedReader(new FileReader(snpFile));
		String line = br.readLine();
		
		int prevChromosome = 0;
		int currChromosomeCount = 0;

		while ((line=br.readLine()) != null)
		{
	 		String[] snpInfo = line.split("\\t");
	 		int chromosome = Integer.parseInt(snpInfo[1].replaceAll("\\D+",""));
	 		int genomePosition = Integer.parseInt(snpInfo[2]);
	 		
	 		if(prevChromosome != chromosome){
	 			prevChromosome = chromosome;
	 			currChromosomeCount = 0;
	 		}
	 		
	 		addEntryToMap(chromosome, genomePosition, currChromosomeCount);
	 		currChromosomeCount++;
		}
		
		br.close();
	}
	
	private void addEntryToMap(int chr, int genomeLoc, int index){
		Map<Integer,Integer> map = chromosomeSnpsMap.get(chr);
		map.put(genomeLoc, index);
	}
	
	public int getChromosomeSnps(int chromosome, int genomeLoc){
		return chromosomeSnpsMap.get(chromosome).get(genomeLoc);
	}
}