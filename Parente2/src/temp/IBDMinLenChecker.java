package temp;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;


public class IBDMinLenChecker {
	
	private String fileToReadPath;

	public IBDMinLenChecker(String fileToReadPath) {
		this.fileToReadPath = fileToReadPath;
	}

	public int read(){
		int minIbdLen = Integer.MAX_VALUE;
		
		try(FileReader fr = new FileReader(fileToReadPath)) {
			try(BufferedReader br = new BufferedReader(fr)) {
				String line;
				line = br.readLine();
				
				while ((line = br.readLine()) != null && (line.length()>0))
				{
					String[] chrValues = line.split("\\s+");
					int loc1 = Integer.parseInt(chrValues[3]);
					int loc2 = Integer.parseInt(chrValues[5]);
					
					int diff = Math.abs(loc2-loc1);
					if(diff>0 && diff<minIbdLen) minIbdLen = diff;
					if(diff>0 && diff < 1000)
						System.out.println("This is Amazing!!!");
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		return minIbdLen;
	}
	
	public static void main(String[] args) {
		//IBDMinLenChecker ibdMinLenChecker = new IBDMinLenChecker("C:\\Users\\user\\GoogleDrive\\gen1\\Genetics\\results\\ibdresults1");
		//System.out.println(ibdMinLenChecker.read());
		IBDMinLenChecker ibdMinLenChecker2 = new IBDMinLenChecker("E:/Gal/University/HGC/workspace/Parente2\\scripts\\parente2 results\\more_data\\ibdresults.13");
		System.out.println(ibdMinLenChecker2.read());
	}
}
