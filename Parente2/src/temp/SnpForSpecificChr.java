package temp;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class SnpForSpecificChr {
	private String fileToReadPath;
    public FileWriter fw;
    String pattern = "chr3";

    public SnpForSpecificChr(String fileToReadPath) {
        this.fileToReadPath = fileToReadPath;
        
        try {
            this.fw = new FileWriter(fileToReadPath+"."+pattern);
        } catch (IOException e) {
            e.printStackTrace();
        }
            
    }

    public void readChr1Results(){
        //p1 p2 chr1 pos1 chr2 pos2
        try(FileReader fr = new FileReader(fileToReadPath)) {
            try(BufferedReader br = new BufferedReader(fr)) {
                String line;
                line = br.readLine();
                fw.append(line+System.lineSeparator());
                
                while ((line = br.readLine()) != null && (line.length()>0))
                {
                    String[] chrValues = line.split("\\s+");
                    String chr = chrValues[1];
                    
                    if(chr.equals(pattern)){
                        fw.append(line+System.lineSeparator());
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }
    
    public static void main(String[] args) {
        String path = "C:\\Users\\GalDu_000\\git\\hgc-parente22\\Parente2\\scripts\\parente2 results\\data1\\snpinfo1";
        SnpForSpecificChr ibdResultForChr1 = new SnpForSpecificChr(path);
        ibdResultForChr1.readChr1Results();
        try {
            ibdResultForChr1.fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
