package temp;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class ResultRestricter {

	private String resultFilePath;
	private String snpInfoFilePath;
	private int minIbdLen;
	private SnpInfoReaderForRestriction snpInfoReader;
	private FileWriter fw;
	
	public ResultRestricter(String resultFilePath, int minIbdLen, String snpInfoFilePath, String outputPath) throws IOException{
		this.resultFilePath = resultFilePath;
		this.minIbdLen = minIbdLen;
		this.snpInfoFilePath = snpInfoFilePath;
		this.fw = new FileWriter(outputPath);
		fw.append("name1\tname2\tchr_start\tstart\tchr_end\tend\n");
	}
	
	private void fillSnpInfo() throws NumberFormatException, IOException{
		snpInfoReader = new SnpInfoReaderForRestriction();
		snpInfoReader.fillSnpInfo(snpInfoFilePath);
	}
	
	public void restrict() throws IOException{
		fillSnpInfo();
		
		try(FileReader fr = new FileReader(resultFilePath)) {
			try(BufferedReader br = new BufferedReader(fr)) {
				String line = br.readLine();
				
				while ((line = br.readLine()) != null && (line.length()>0))
				{
					String[] chrValues = line.split("\\s+");
			 		int chr1 = Integer.parseInt(chrValues[2].substring(3));
			 		int chr2 = Integer.parseInt(chrValues[4].substring(3));
			 		
			 		if(chr1!=chr2){
			 			System.out.println("There is row with diffrenet chromosomes");
			 		}
			 		
			 		int genome1 = Integer.parseInt(chrValues[3]);
			 		int genome2 = Integer.parseInt(chrValues[5]);
			 		
			 		if(genome1<0){
			 			System.out.println("There is negative genome position: "+genome1);
			 		}
			 		
			 		if(genome2<0){
			 			System.out.println("There is negative genome position: "+genome2);
			 		}
			 		
			 		int ind1 = snpInfoReader.getChromosomeSnps(chr1, genome1);
			 		int ind2 = snpInfoReader.getChromosomeSnps(chr2, genome2);
			 		
			 		if(Math.abs(ind2-ind1)>=minIbdLen){
			 			writeOutput(line);
			 		}
				}
			}
		}
		
		fw.close();
	}
	
	private void writeOutput(String line) throws IOException{
		fw.append(line+"\n");
	}
	
	public static void main(String[] args) {
		String resultFilePath = args[0];
		String outputFilePath = args[1];
		String snpInfoFilePath = args[2];
		int minIbdLen = Integer.parseInt(args[3]);
		try {
			ResultRestricter resultRestricter = new ResultRestricter(resultFilePath, minIbdLen, snpInfoFilePath, outputFilePath);
			resultRestricter.restrict();
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("error");
			System.exit(-1);
		}
	}
}
