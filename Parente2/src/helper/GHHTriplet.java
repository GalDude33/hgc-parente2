package helper;

public class GHHTriplet {

	public final  short g;
	public final short h1;
	public final short h2;
	
	public GHHTriplet(short g, short h1, short h2) {
		this.g = g;
		this.h1 = h1;
		this.h2 = h2;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + g;
		result = prime * result + h1;
		result = prime * result + h2;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GHHTriplet other = (GHHTriplet) obj;
		if (g != other.g)
			return false;
		if (h1 != other.h1)
			return false;
		if (h2 != other.h2)
			return false;
		return true;
	}
}
