package helper;

public class HapFreqPair {
	public final short h;
	public final double f;
	
	public HapFreqPair(short h, double f, int hCount, int totHapNum) {
		this.h = h;
		this.f = f;
	}
}
