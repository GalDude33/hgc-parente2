package helper;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Field;

import parente.ConfigInfo;

public class SettingsGetter {

	private final String filePath;

	public SettingsGetter(String filePath) {
		this.filePath = filePath;
	}

	public void fillSettings() throws Exception {
		try (FileReader fr = new FileReader(filePath)) {
			try (BufferedReader br = new BufferedReader(fr)) {
				String line;
				
				while ((line = br.readLine()) != null && (line.length()>0)){
					String[] values = line.split("=");
				    Field field;
					try {
						field = ConfigInfo.class.getField(values[0]);
						if(values[1].contains(".")){
							field.set(null, Double.parseDouble(values[1]));
						}else{
							field.set(null, Integer.parseInt(values[1]));
						}
					} catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
						e.printStackTrace();
						throw e;
					} 
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
			throw e;
		} 
	}
}
