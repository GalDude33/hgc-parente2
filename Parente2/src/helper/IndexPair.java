package helper;

public class IndexPair {
	    public final int first;
	    public final int second;
	    public final int chrNum;
	    
	    public IndexPair(int first, int second, int chrNum) {
	    	super();
	    	this.first = Math.min(first, second);
	    	this.second = Math.max(first, second);
	    	this.chrNum = chrNum;
	    }

	    public int hashCode() {
	    	int hashFirst = Integer.hashCode(first);
	    	int hashSecond = Integer.hashCode(second);

	    	return (hashFirst + hashSecond) * hashSecond + hashFirst;
	    }

	    public boolean equals(Object other) {
	    	if (other instanceof IndexPair) {
	    		IndexPair otherPair = (IndexPair) other;
	    		return this.first == otherPair.first
	    				&& this.second == otherPair.second;
	    	}

	    	return false;
	    }

	    public String toString()
	    { 
	           return "(" + first + ", " + second + ")"; 
	    }
}
