package helper;

public class ScoreTriplet {

	public final int firstPerInd;
	public final int secondPerInd;
	public final int windowId;
	
	public ScoreTriplet(int firstPerInd, int secondPerInd, int windowId) {
		this.firstPerInd = firstPerInd;
		this.secondPerInd = secondPerInd;
		this.windowId = windowId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + firstPerInd;
		result = prime * result + secondPerInd;
		result = prime * result + windowId;
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ScoreTriplet other = (ScoreTriplet) obj;
		if (firstPerInd != other.firstPerInd)
			return false;
		if (secondPerInd != other.secondPerInd)
			return false;
		if (windowId != other.windowId)
			return false;
		return true;
	}
}
