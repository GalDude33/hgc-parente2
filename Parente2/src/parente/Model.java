package parente;

import helper.HapFreqPair;
import java.util.ArrayList;


public class Model {
	private byte[] _g1;
	private byte[] _g2;
	private HaplotypesData haplotypesData;
	private final float geno_error = (float) ConfigInfo.genotypeError;
	private GHHCache ghhCache;

	public Model(byte[] g1, byte[] g2, HaplotypesData hapData, GHHCache ghhCache) {
		this._g1 = g1;
		this._g2 = g2;
		this.haplotypesData = hapData;
		this.ghhCache = ghhCache;
	}

	//log(exp(a) + exp(b)) = a + log(1 + exp(b-a))
	double LogAdd(final double num1, final double num2) {
		if (num1 > num2)
			return Math.log(Math.exp(num2) + Math.exp(num1));//Math.log1p(Math.exp(num2 - num1)) + num1;
		else if (num1 < num2)
			return Math.log(Math.exp(num2) + Math.exp(num1));//Math.log1p(Math.exp(num1 - num2)) + num2;

		return Math.log(2) + num1; // 2 * num1
	}

	double logProbGGivenHH(Genotype geno, short h1, short h2, float error_rate) {
		//GHHTriplet ghhTriplet = new GHHTriplet(geno.asShort,h1,h2);
		double prob = ghhCache.getGHHProb(geno.asShort, h1, h2);
		
		if(prob<0){
			prob = Math.log(1); 
			final int genoSize = geno.size();
			
			for (int i = 0; i < genoSize; ++i) {
				if (((h1 >> i) & 1) + ((h2 >> i) & 1) == geno.get(i)) {
					prob += Math.log(1.0 - error_rate);
				}
				else {
					prob += Math.log(error_rate / 2);
				}
			}
			
			ghhCache.addGHHProb(geno.asShort, h1, h2, prob);
		} 
		
		return prob;
	}
	
	public double CalcIBDModel(ArrayList<HapFreqPair> hapProbMap, Genotype g1, Genotype g2) {
		double log_prob_rel = Math.log(0);

		// P(g1, g2 | IBD) = \sum_{h1,h2,h3} P(g1 | h1,h2) * P(g2 | h2,h3) * P(h1) * P(h2) * P(h3)
		final int hapProbMapSize = hapProbMap.size();
		HapFreqPair tmpHapFreqPair;
		for (int i1=0;i1<hapProbMapSize;i1++) {
			tmpHapFreqPair = hapProbMap.get(i1);
			final short h1 = tmpHapFreqPair.h;
			final double log_prob_h1 = tmpHapFreqPair.logF;

			for (int i2=0;i2<hapProbMapSize;i2++) {
				tmpHapFreqPair = hapProbMap.get(i2);
				final short h2 = tmpHapFreqPair.h;
				final double log_prob_h2 = tmpHapFreqPair.logF;

				// P(g1 | h1,h2)
				double log_prob_g1_given_hh = logProbGGivenHH(g1, h1, h2, geno_error);

				for (int i3=0;i3<hapProbMapSize;i3++) {
					tmpHapFreqPair = hapProbMap.get(i3);
					final short h3 = tmpHapFreqPair.h;
					final double log_prob_h3 = tmpHapFreqPair.logF;

					// P(g2 | h2,h3)
					double log_prob_g2_given_hh = logProbGGivenHH(g2, h2, h3, geno_error);

					double log_prob_ghhh = log_prob_g1_given_hh + log_prob_g2_given_hh + log_prob_h1 + log_prob_h2 + log_prob_h3;

					log_prob_rel = LogAdd(log_prob_rel, log_prob_ghhh);
				}
			}
		}

		return log_prob_rel;
	}
	
	public double CalcNonIBDModel(final ArrayList<HapFreqPair> hapProbMap, Genotype g1, Genotype g2) {
		double log_prob_unr1 = Math.log(0); // p(g1) = \sum_{h,h} p(g1|hh) p(h) p(h)
	    double log_prob_unr2 = Math.log(0); // p(g2) = \sum_{h,h} p(g1|hh) p(h) p(h)
	    final int hapMapSize = hapProbMap.size();
	    
	    // P(g1, g2 | IBD) = \sum_{h1,h2,h3} P(g1 | h1,h2) * P(g2 | h2,h3) * P(h1) * P(h2) * P(h3)
		for (int i1 = 0; i1 < hapMapSize; i1++) {
			final HapFreqPair sdPair1 = hapProbMap.get(i1);
			final short h1 = sdPair1.h;
			final double log_prob_h1 = sdPair1.logF;

	      // Do upper right part of the matrix, we will multiply by 2
			for (int i2 = i1; i2 < hapMapSize; i2++) {
				final HapFreqPair sdPair2 = hapProbMap.get(i2);
				final short h2 = sdPair2.h;
				final double log_prob_h2 = sdPair2.logF;
				// P(g1 | h1,h2)
				double log_prob_g1_given_hh = logProbGGivenHH(g1,h1,h2,geno_error);

				// P(g2 | h1,h2)
				double log_prob_g2_given_hh = logProbGGivenHH(g2,h1,h2,geno_error);

				double lp_hh = log_prob_h1 + log_prob_h2;
				double log_prob_ghhh1 = log_prob_g1_given_hh + lp_hh;
				double log_prob_ghhh2 = log_prob_g2_given_hh + lp_hh;

	        // If we are not on the diagonal, multiply by 2
	        if (i1 != i2)
	        {
	          log_prob_ghhh1 += Math.log(2);
	          log_prob_ghhh2 += Math.log(2);
	        }

	        // Add to p(g1) and p(g2)
	        log_prob_unr1 = LogAdd(log_prob_unr1, log_prob_ghhh1);
	        log_prob_unr2 = LogAdd(log_prob_unr2, log_prob_ghhh2);
	      }
	    }
		
		return log_prob_unr1 + log_prob_unr2; // p(g1 g2 | hhhh) = p(g1|hh) * p(g2|hh)
	}
	
	public double Calculate(Window win) {
		final Genotype g1 = win.Apply(this._g1);
		final Genotype g2 = win.Apply(this._g2);

		final ArrayList<HapFreqPair> hapProbMap = hapProbsForWindow(win);

		// return log(P_I / P_I_NOT) == log(P_I) - log(P_I_NOT)
		return (CalcIBDModel(hapProbMap, g1, g2) - CalcNonIBDModel(hapProbMap, g1, g2))/Math.log(10);
	}

	private ArrayList<HapFreqPair> hapProbsForWindow(Window win) {
		return haplotypesData.getHapFreq(win);
	}
}