package parente;

import java.util.Arrays;

public class GHHCache {

	//private Map<GHHTriplet, Double> ghhMap = new HashMap<GHHTriplet, Double>();
	
	public final double[][] ghhCache;
	private final int hapLen;
	
	public GHHCache(int genoLen, int hapLen) {
		this.hapLen = hapLen;
		ghhCache = new double[(int) Math.pow(3, genoLen)][(int) Math.pow(Math.pow(2, hapLen), 2)];
		
		// init all to negative
		for(double[] row : ghhCache)
			Arrays.fill(row, -33d);
	}
	
	private int getKeyFromHaps(short h1, short h2) {
		//final short firstHap = h1<h2 ? h1 : h2;
		//final short secHap = h1<h2 ? h2 : h1;
		return (h1<h2 ? h1 : h2) + ((h1<h2 ? h2 : h1) << hapLen);
	}
	
	public Double getGHHProb(short g, short h1, short h2){
		return ghhCache[g][getKeyFromHaps(h1,h2)];
	}
	
	public void addGHHProb(short g, short h1, short h2, double prob){
		ghhCache[g][getKeyFromHaps(h1,h2)] = prob;
	}
}
