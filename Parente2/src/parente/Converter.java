package parente;

public interface Converter {
	public int convert(int input);
}
