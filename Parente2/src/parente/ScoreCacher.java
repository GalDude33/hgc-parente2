package parente;

import helper.ScoreTriplet;

import java.util.HashMap;
import java.util.Map;

public class ScoreCacher {

	private Map<ScoreTriplet,Boolean> scoreCache = new HashMap<ScoreTriplet,Boolean>();
	
	public void addScore(ScoreTriplet triplet, Boolean score){
		scoreCache.put(triplet, score);
	}
	
	public Boolean getScore(ScoreTriplet triplet){
		Boolean res = scoreCache.get(triplet);
		
		if(res!=null){
			scoreCache.remove(triplet);
		}
		
		return res;
	}
	
	public void emptyCache(){
		scoreCache.clear();
	}
}
