package parente;
import java.util.Arrays;
import java.util.List;


public class Statistics 
{
    double[] data;
    double size;    
    
    public Statistics(double[] data) 
    {
        this.data = data;
        size = data.length;
        //this.p = new Percentile();
    }   
    
    public Statistics(List<Double> data) 
    {
    	double[] d = new double[data.size()];
    	int i = 0;
    	for (double e : data){
			d[i++]=e;
    	}
        this.data = d;
        size = d.length;
    }  
    
    public double getMean()
    {
        double sum = 0.0;
        for(double a : data){
            sum += a;
        }
        return sum/size;
    }

    public double getVariance()
    {
        double mean = getMean();
        double temp = 0;
        for(double a : data){
            temp += Math.pow(mean-a, 2);
        }
        return temp/size;
    }

    public double getStdDev()
    {
        return Math.sqrt(getVariance());
    }

    /*// 0<=percentile<=100
    public double percentile(double percentile) 
    {
    	return p.evaluate(data, percentile);
    }*/
    
    public double median() 
    {
       double[] b = new double[data.length];
       System.arraycopy(data, 0, b, 0, b.length);
       Arrays.sort(b);

       if (data.length % 2 == 0) 
       {
          return (b[(b.length / 2) - 1] + b[b.length / 2]) / 2.0;
       } 
       else 
       {
          return b[b.length / 2];
       }
    }
    
    public double getMax()
    {
    	double max = data[0];
    	for (int i = 0; i < data.length; i++){
			if(data[i]>max){
				max = data[i];
			}
    	}
    	return max;
    }
    
    public double getMin()
    {
    	double min = data[0];
    	for (int i = 0; i < data.length; i++){
			if(data[i]<min){
				min = data[i];
			}
    	}
    	return min;
    }
}

