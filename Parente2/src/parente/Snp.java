package parente;

public class Snp {

	//private int id;
	//private int chromosome;
	private final int genomePosition;
	
	public Snp(/*int id, int chromosome,*/ int genomePosition) {
		//this.id = id;
		//this.chromosome = chromosome;
		this.genomePosition = genomePosition;
	}
	
	/*public int getId() {
		return id;
	}
	
	public int getChromosome() {
		return chromosome;
	}*/
	
	public int getGenomePosition() {
		return genomePosition;
	}
}
