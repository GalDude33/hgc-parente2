package parente;


public class GammaCalculator {
	private final WindowSubset _windowSubset;
	private final HaplotypesData haplotypesData;
	//private ScoreCacher scoreCacher;
	private final GHHCache ghhCache; 
	
	public GammaCalculator(WindowSubset windowSubset, HaplotypesData haplotypesData/*, ScoreCacher scoreCacher*/, GHHCache ghhCache) {
		this._windowSubset = windowSubset;
		this.haplotypesData = haplotypesData;
		//this.scoreCacher = scoreCacher;
		this.ghhCache = ghhCache;
	}
	
	/*public final static double log2 = Math.log(2);
	public static double log2(double val) {
		return Math.log(val)/log2;
	}*/
	
	public double Calculate(byte[] g1, byte[] g2, int p1Ind, int p2Ind) {
		double gamma = 0;
		ModelCopyWithMul model = new ModelCopyWithMul(g1, g2, haplotypesData, ghhCache);

		for(Window win : _windowSubset.GetWindows()) {
			//ScoreTriplet scoreTriplet = new ScoreTriplet(p1Ind, p2Ind, win.getId());
			//Boolean score = scoreCacher.getScore(scoreTriplet); 
			//if(score==null){
				WindowStatistics winS = win.getWinStatistics();
				//score = model.Calculate(win) >= winS.getMean_r()-2*Math.abs(winS.getSd_r()) ? true : false;
				//scoreCacher.addScore(scoreTriplet, score);
			//}
			gamma += model.Calculate(win) >= winS.getMean_r()-2*Math.abs(winS.getSd_r()) ? 1 : 0;
		}
		
		return gamma/_windowSubset.GetWindows().size();
	}
	
	
	/*public static class genProbabiltyHelper {
		
		private static double _genError = ConfigInfo.genotypeError;
		
		public static double genProbGivenAlleles(byte gen, byte a1, byte a2) {
			return (gen == a1 + a2) ? (1 - _genError) :  _genError/2;
		}
		
		public static double genProbGivenHaps(Genotype genotype, HaplotypePair haplotypePair1) {
			double prob = 1;
			
			byte[] h1 = haplotypePair1.getFirst();
			byte[] h2 = haplotypePair1.getSecond();
			for(int i=0; i<genotype.size();i++) {
				// genProbGivenAlleles(genotype.get(i), h1[i], h2[i]);
				prob *= (genotype.get(i) == (h1[i] + h2[i])) ? (1 - _genError) :  _genError/2;
			}
			
			return prob;
		}
	}*/
}
