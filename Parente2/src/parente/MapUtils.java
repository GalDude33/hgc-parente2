package parente;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;


public class MapUtils<T,V> {
	
	public Map<T,List<V>> addEntryToMap(Map<T,List<V>> map, T key, V value) {
		if (map.containsKey(key)) {
			map.get(key).add(value);
		} else {
			List<V> newCollection = new ArrayList<V>();
			newCollection.add(value);
			map.put(key, newCollection);
		}
		
		return map;
	}
	
	public Map<T,List<V>> addEntriesToMapFromMap(Map<T,List<V>> map, Map<T,List<V>> otherMap) {
		for(Entry<T,List<V>> entry : otherMap.entrySet()){
			T key = entry.getKey();
			List<V> value = entry.getValue();
			
			if (map.containsKey(key)) {
				map.get(key).addAll(value);
			} else {
				List<V> newCollection = new ArrayList<V>();
				newCollection.addAll(value);
				map.put(key, newCollection);
			}
		}
		
		return map;
	}
}
