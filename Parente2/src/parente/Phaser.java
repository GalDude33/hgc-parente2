package parente;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Phaser {

	/**
	 * 
	 * @param path to haplotypes# file
	 * phase the input haplotypes
	 * 	1. create 'unphased' dir
	 *  2. put haplotypes# and snpinfo# in 'unphased' dir
	 *  3. call the script with arguments- 
	 *  	full path to working dir
	 *  	filename (depends on name in 2)
	 *  	snpinfo file name (depends on name in 2)
	 * @return paths to all '.hap' and '.geno' files (one for each chromosome)
	 */
	public static String[] phaseFromGenoFile(String path,String scriptPath){
		String[] splitted = path.split("/");
		String dataFileName = splitted[splitted.length-1];
		Pattern pattern = Pattern.compile("[0-9]+");
		Matcher matcher = pattern.matcher(dataFileName);
		matcher.find();
		
		String workDirPath="";
		for (int i = 0; i < splitted.length-1; i++)
			workDirPath+=splitted[i]+"/";
		String scriptDirPath="";
		splitted = scriptPath.split("/");
		for (int i = 0; i < splitted.length-1; i++)
			scriptDirPath+=splitted[i]+"/";
		
		String snpinfoFileName="snpinfo"+matcher.group(0);
		
		new File(workDirPath.replaceAll("\\s", "\\ ")+"unphased").mkdir();
		String path1 = workDirPath.replaceAll("\\s", "\\ ");
		String path2 = scriptDirPath.replaceAll("\\s", "\\ ");
		
		String command[]= {"/bin/bash",scriptPath, path1,dataFileName,snpinfoFileName,path2};
		
		Process p;
        try {
        	Files.createSymbolicLink(new File(workDirPath.replaceAll("\\s", "\\ ")+"unphased/"+snpinfoFileName).toPath(),
					new File(workDirPath.replaceAll("\\s", "\\ ")+snpinfoFileName).toPath());
        	Files.createSymbolicLink(new File(workDirPath.replaceAll("\\s", "\\ ")+"unphased/"+dataFileName).toPath(),
					new File(workDirPath.replaceAll("\\s", "\\ ")+dataFileName).toPath());
			p = new ProcessBuilder(command).start();
			
			
			BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
		    String line;

		    System.out.println("Output of running " + command + " is: ");
		    while ((line = br.readLine()) != null)
		        System.out.println(line);
		        
		        
			p.waitFor();
	        p.destroy();
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
			System.exit(-1);
		}
        
		String[] paths = new String[23];
		for (int i = 0; i < paths.length; i++)
			paths[i]=workDirPath+"/phased-without-missing/"+dataFileName+(i+1);
		return paths;
	}
	public static void main2(String[] args) {
		phaseFromGenoFile("/media/ubuntu/Stuff C/CHG2015/Tests/haplotypes1","/home/ubuntu/CHG2015/Code/hgc-parente2/Parente2/scripts/PhaseScript");
	}
}
