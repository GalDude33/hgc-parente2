package parente;
import java.io.IOException;
import java.util.Iterator;


public class BlockGenerator implements Iterable<Block>, Iterator<Block> {

	//private SnpInfoReader snpInfoReader;
	private final int chromosome;
	private final int chromosomeOffset;
	private final BlockIndexGenerator blockIndexGenerator;
	private final BlockReader blockReader;
	
	public BlockGenerator(String genotypeFilePath, SnpInfoReader snpInfoReader, int chromosome) throws IOException {
		//this.snpInfoReader = snpInfoReader;
		this.chromosome = chromosome;
		this.chromosomeOffset = snpInfoReader.getChromosomeOffset(chromosome);
		this.blockReader = new BlockReader(genotypeFilePath, snpInfoReader, chromosome);
		this.blockIndexGenerator = new BlockIndexGenerator(snpInfoReader, chromosome);
	}

	@Override
	public boolean hasNext() {
		return blockIndexGenerator.hasNext();
	}

	@Override
	public Block next() {
		// TODO: consider avoiding small too blocks
		int[] blockBorders = blockIndexGenerator.next();
		//int listSize = blockBorders[1]-blockBorders[0]+1;
		//Snp[] blockSnps = snpInfoReader.getChromosomeSnps(chromosome).subList(blockBorders[0], blockBorders[1]+1).toArray(new Snp[listSize]);
		try {
			return new Block(blockReader.readBlock(blockBorders[0], blockBorders[1])/*, blockSnps*/, chromosomeOffset, blockBorders, chromosome);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(-1);
		}
		return null;		
	}

	@Override
	public Iterator<Block> iterator() {
		return this;
	}
}
