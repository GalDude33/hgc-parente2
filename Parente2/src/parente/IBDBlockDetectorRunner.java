package parente;

import helper.IndexPair;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;

public class IBDBlockDetectorRunner implements Runnable {

	private Block block;
	// private HaplotypesData haplotypesData;
	private Queue<Map<IndexPair, List<int[]>>> resQueue;
	private AtomicInteger atomicInt;
	final Logger logger = Logger.getLogger(IBDBlockDetectorRunner.class.getName());
	private Chromosome chr;
	private final String haplotypeFilePath;
	private GHHCache ghhCache;

	public IBDBlockDetectorRunner(String haplotypeFilePath, Chromosome chr, Block block,
			ConcurrentLinkedQueue<Map<IndexPair, List<int[]>>> resultQueue, AtomicInteger atomicInt, GHHCache ghhCache) {
		this.haplotypeFilePath = haplotypeFilePath;
		this.chr = chr;
		this.block = block;
		this.resQueue = resultQueue;
		this.atomicInt = atomicInt;
		this.ghhCache = ghhCache;
	}

	@Override
	public void run() {
		try {
			HaplotypesData haplotypesData = null;
			try {
				haplotypesData = new HaplotypesData(haplotypeFilePath, block.getChromosomeOffset(),
						block.getBlockBorders());
			} catch (IOException | InterruptedException e) {
				e.printStackTrace();
				throw e;
			}

			IBDBlockDetector ibdBlockDetector = new IBDBlockDetector(block, haplotypesData, chr, ghhCache);
			// add to output
			Map<IndexPair, List<int[]>> blockResMap = ibdBlockDetector.Detect();

			resQueue.add(blockResMap);

			logger.log(Level.INFO, ConfigInfo.logMessage, new Object[] { Thread.currentThread().getName(),
					"done detective job..." });
			atomicInt.decrementAndGet();
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(-1);
		}
	}
}