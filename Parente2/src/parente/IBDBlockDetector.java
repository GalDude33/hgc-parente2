package parente;
import helper.IndexPair;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;


public class IBDBlockDetector {
	
	private final Block block;
	private final HaplotypesData haplotypesData;
	private MapUtils<IndexPair,int[]> mapUtils = new MapUtils<IndexPair,int[]>();
	//public static final Logger logger = Logger.getLogger(IBDBlockDetector.class.getName());
	final static String logMessage = "[{0}]: {1}";
	private final Merger merger;
	//private int blockOffset;
	private final Chromosome chromosome;
	//private ScoreCacher scoreCacher = new ScoreCacher();
	private final GHHCache ghhCache;
	
	public IBDBlockDetector(Block block, HaplotypesData haplotypesData, Chromosome chromosome, GHHCache ghhCache) {
		this.block = block;
		this.haplotypesData = haplotypesData;
		this.merger = new Merger(); 
		//this.blockOffset = block.getBlockBorders()[0];
		this.chromosome = chromosome;
		this.ghhCache = ghhCache;
	}
	
	public Map<IndexPair, List<int[]>> Detect() throws IOException {
		byte[][] _genotypeMatrix = block.getGenotypes();

		Window[] filteredWins;
		{
			Window[] wins = (new WindowGenerator().generateWindows(block)).toArray(new Window[0]);
			haplotypesData.getWindowHaplotypesFrequency(wins);
			filteredWins = new TrainData(haplotypesData, ConfigInfo.numOfPairs, wins, ConfigInfo.keepFraction, ghhCache).run();
		}
		
		List<WindowSubset> windowSubsets = new WindowSubsetGenerator().generateWindowSubsets(block, Arrays.asList(filteredWins));
		
		Map<IndexPair,List<int[]>> ibdPairs = new HashMap<IndexPair,List<int[]>>();
		Map<IndexPair,List<int[]>> ibdPairsForChr = new HashMap<IndexPair,List<int[]>>();
		
		SimpleDateFormat timeFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		int blockOffset = getBlockOffset();
		int prog = 1;
		int total_prog = windowSubsets.size();
		for(WindowSubset windowSubset : windowSubsets){
			//logger.log(Level.INFO, logMessage, new Object[]{Thread.currentThread().getName(),"Started working on windowSubset..."});
			
			GammaCalculator gammaCalc = new GammaCalculator(windowSubset, haplotypesData/*, scoreCacher*/, ghhCache);
			
			// Iterate over all pairs
			for(int i=0;i<_genotypeMatrix.length;i++) {
				byte[] g1 = _genotypeMatrix[i];
				for(int j=i+1;j<_genotypeMatrix.length;j++) {
					byte[] g2 = _genotypeMatrix[j];
					double gamma = gammaCalc.Calculate(g1, g2, i, j);
					
					if(gamma >= ConfigInfo.gammaThreshold/*0.79 windowSubset.getThreshold()*/) {
						mapUtils.addEntryToMap(ibdPairs, new IndexPair(i,j, block.getChromosomeNum()), 
								new int[]{blockOffset + windowSubset.GetLeftMostMarker(), blockOffset + windowSubset.GetRightMostMarker()});
					}	
				}
			}
			
			if(prog%ConfigInfo.logWriteGap==0) {
				String time = timeFormat.format(new Date());
				System.out.println(Thread.currentThread().getName()+", IBDBlockDetector::Detect - Chr "+ chromosome.num +" Done WindowSubset "+prog+"/"+total_prog+" "+time);
				//System.out.println(String.format("%s , IBDBlockDetector::Detect - Done WindowSubset %d/%d %t", Thread.currentThread().getName(), prog, total_prog, System.currentTimeMillis()));
			}
			++prog;
		}
		
		merger.merge(ibdPairs);
		
		int[] blockBorders = block.getBlockBorders();
		
		Iterator<Entry<IndexPair,List<int[]>>> itr = ibdPairs.entrySet().iterator();
		
		//for(Entry<IndexPair,List<int[]>> ibdEntry : ibdPairs.entrySet()) {
		while(itr.hasNext()){
			Entry<IndexPair,List<int[]>> ibdEntry = itr.next();
			List<int[]> twoIndsIBDs = ibdEntry.getValue();
			List<int[]> listToBeAdded = new ArrayList<int[]>();
			
			int size = twoIndsIBDs.size();
			if(size>0 && twoIndsIBDs.get(0)[0]==blockBorders[0]) {
				int[] res = twoIndsIBDs.remove(0);
				listToBeAdded.add(res);
			}
			
			size = twoIndsIBDs.size();
			if(size>0 && twoIndsIBDs.get(size-1)[1]==blockBorders[1]) {
				int[] res = twoIndsIBDs.remove(size-1);
				listToBeAdded.add(res);
			}
			
			if(!listToBeAdded.isEmpty()){
				ibdPairsForChr.put(ibdEntry.getKey(), listToBeAdded);
			}
			
			if(twoIndsIBDs.isEmpty()){
				itr.remove();
			}
		}
		
		convertToGenomeLocations(ibdPairs);
		chromosome.addBlockRes(ibdPairsForChr);
		//scoreCacher.emptyCache();
		
		return ibdPairs;
	}

	public int getBlockOffset(){
		return block.getBlockBorders()[0];
	}
	
	private void convertToGenomeLocations(Map<IndexPair, List<int[]>> ibdPairs) {
		chromosome.convertToGenomeLocations(ibdPairs);
		
		/*Snp[] snpInfo = chromosome.get;
		for(List<int[]> currList : ibdPairs.values()) {
			for(int[] pair : currList) {
				pair[0] = snpInfo[pair[0]].getGenomePosition();
				pair[1] = snpInfo[pair[1]].getGenomePosition();
			}
		}*/
	}
}
