package parente;

import helper.IndexPair;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Map.Entry;
import java.util.concurrent.atomic.AtomicInteger;


public class Chromosome {

	public int num;
	private List<Snp> snpList;
	private AtomicInteger numOfRes = new AtomicInteger(0);
	//private Map<IndexPair,List<int[]>> ibdPairs = new HashMap<IndexPair,List<int[]>>();
	private int blockNum;
	private Merger merger = new Merger(); 
	private MapUtils<IndexPair,int[]> mapUtils = new MapUtils<IndexPair,int[]>();
	private Queue<Map<IndexPair, List<int[]>>> resQueue;
	private FileWriter chrOutputFile;
	private String chrOutputPath;
	
	public Chromosome(int num, SnpInfoReader snpInfo, Queue<Map<IndexPair, List<int[]>>> resQueue, String chrOutputPath) throws IOException{
		this.num = num;
		this.snpList = snpInfo.getChromosomeSnps(num);
		this.resQueue = resQueue;
		this.chrOutputPath = chrOutputPath;
		try {
			this.chrOutputFile = new FileWriter(chrOutputPath);
		} catch (IOException e) {
			e.printStackTrace();
			throw e;
		}
	}
	
	public void setBlocksNum(int blockNum) {
		this.blockNum = blockNum;
	}
	
	public void addBlockRes(Map<IndexPair,List<int[]>> blockRes) throws IOException {
		try {
			writeOutput(blockRes);
		} catch (IOException e) {
			e.printStackTrace();
			throw e;
		}
		
		//mapUtils.addEntriesToMapFromMap(ibdPairs, blockRes);
		int resNum = numOfRes.incrementAndGet();
		
		if(resNum==blockNum){
			try {
				chrOutputFile.close();
			} catch (IOException e) {
				e.printStackTrace();
				throw e;
			}
			
			Map<IndexPair,List<int[]>> ibdPairs = readChrOutput();
			//call merge on all chrom
			merger.merge(ibdPairs);
			convertToGenomeLocations(ibdPairs);
			resQueue.add(ibdPairs);
			String timeFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").format(new Date());
			System.out.println("Done detecting chr "+num+" "+timeFormat);
			
			nullEverything();
		}
	}
	
	private void nullEverything() {
		mapUtils = null;
		resQueue = null;
		snpList = null;
		merger = null;
		chrOutputPath = null;
		numOfRes = null;
	}
	
	public void convertToGenomeLocations(Map<IndexPair, List<int[]>> ibdPairs) {
		
		Iterator<Entry<IndexPair,List<int[]>>> itr = ibdPairs.entrySet().iterator();
		
		while(itr.hasNext()){
			Entry<IndexPair,List<int[]>> ibdEntry = itr.next();
			
			/*Iterator<int[]> itr1 = ibdEntry.getValue().iterator();
			
			while(itr1.hasNext()){
				int[] currPair = itr1.next();
				if(currPair[1]-currPair[0]<ConfigInfo.minIbdLength){
					itr1.remove();
				} else{
					currPair[0] = snpList.get(currPair[0]).getGenomePosition();
					currPair[1] = snpList.get(currPair[1]).getGenomePosition();
				}
			}*/
			
			List<int[]> pairs = ibdEntry.getValue();
			
			for(int i=pairs.size()-1; i>=0; i--) {
				int[] currPair = pairs.get(i);
				if(currPair[1]-currPair[0]<ConfigInfo.minIbdLength){
					pairs.remove(i);
				} else{
					currPair[0] = snpList.get(currPair[0]).getGenomePosition();
					currPair[1] = snpList.get(currPair[1]).getGenomePosition();
				}
			}
			
			if(ibdEntry.getValue().isEmpty()){
				itr.remove();
			}
		}
		
		
		/*for(List<int[]> currList : ibdPairs.values()) {
			for(int[] pair : currList) {
				pair[0] = snpList.get(pair[0]).getGenomePosition();
				pair[1] = snpList.get(pair[1]).getGenomePosition();
			}
		}*/
	}
	
	public synchronized void writeOutput(Map<IndexPair, List<int[]>> currBlockResMap) throws IOException{
		for(Map.Entry<IndexPair, List<int[]>> entry : currBlockResMap.entrySet()) {
			int p1 = entry.getKey().first;
			int p2 = entry.getKey().second;
			
			for(int[] ibdBorders : entry.getValue()) {
				int startLoc = ibdBorders[0];
				int endLoc = ibdBorders[1];
				chrOutputFile.append(p1+"\t"+p2+"\t"+startLoc+"\t"+endLoc+"\n");
			}
		}
	}
	
	public Map<IndexPair,List<int[]>> readChrOutput() throws IOException {
		Map<IndexPair,List<int[]>> ibdPairs = new HashMap<IndexPair,List<int[]>>();
		
		try(FileReader fr = new FileReader(chrOutputPath)) {
			try(BufferedReader br = new BufferedReader(fr)) {
				String line;
				
				while ((line = br.readLine()) != null && (line.length()>0))
				{
					String[] values = line.split("\\s+");
					int p1 = Integer.parseInt(values[0]);
					int p2 = Integer.parseInt(values[1]);
		 			int ibd1Pos = Integer.parseInt(values[2]);
		 			int ibd2Pos = Integer.parseInt(values[3]);
		 			
		 			mapUtils.addEntryToMap(ibdPairs, new IndexPair(p1, p2, num), 
							new int[]{ibd1Pos, ibd2Pos});
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
			throw e;
		}
		
		return ibdPairs;
	}
}
