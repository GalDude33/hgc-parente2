package parente;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class BlockIndexGenerator implements Iterable<int[]>, Iterator<int[]> {
	
	//private int blockSize = ConfigInfo.blockSize;
	private final SnpInfoReader snpInfoReader;
	private final List<Integer> currBlockStartIndexs = new ArrayList<Integer>();
	private final int chromosome;
	private int prevEndIndex = 0;
	private final List<Snp> snpList;
	private final int minBlockSize = ConfigInfo.minBlockSize;
	
	public BlockIndexGenerator(SnpInfoReader snpInfoReader, int chromosome) {
		this.snpInfoReader = snpInfoReader;
		this.chromosome = chromosome;
		this.snpList = snpInfoReader.getChromosomeSnps(chromosome);
	}
	
	@Override
	public boolean hasNext() {
		return prevEndIndex<snpList.size()-1;
	}

	@Override
	public int[] next() {
		int startIndex = 0;
		int genomePos = ConfigInfo.blockSize;
				
		if(!currBlockStartIndexs.isEmpty()){
			startIndex = currBlockStartIndexs.remove(0);
			genomePos = snpList.get(startIndex).getGenomePosition()+ConfigInfo.blockSize;
		}
	
		int endIndex = snpInfoReader.getNearestSnpIndexAtGivenPosition(chromosome, genomePos);
		
		// if next block is too small => absorb it
		if(endIndex - startIndex +1 < minBlockSize){
			endIndex = startIndex + minBlockSize -1;
			endIndex = endIndex-(snpList.size()-1)>0 ? snpList.size()-1 : endIndex;
		}
		
		if((snpList.size()-1) - endIndex < minBlockSize){
			endIndex = snpList.size()-1;
		}
		
		int[] result = new int[]{startIndex, endIndex};
		prevEndIndex = endIndex;
		
		currBlockStartIndexs.add(endIndex+1);
		
		//System.out.println(chromosome + " blockStartIndex= "+startIndex+ " blockEndIndex= "+endIndex);
		
		//genomePos = snpList.get(endIndex).getGenomePosition()+blockSize;
		//chromosomeCurrBlockStartIndex.put(chromosome, new Integer[]{endIndex+1, genomePos});
		
		return result;
	}

	@Override
	public Iterator<int[]> iterator() {
		return this;
	}	
}
