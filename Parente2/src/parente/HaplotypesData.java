package parente;
import helper.HapFreqPair;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

/*
 * Reading and using the phased haplotypes
 */
public class HaplotypesData {

	//NOTE- first index is "individual" (two indexes actually because phased), second is allele index->{0,...,||SNPs||}, NOT physical location
	private final byte[][] haplotypes;
	private Map<Window,HapFreqPair[]>  hapFreq;
	private Map<Window,Map<Short,Integer>>  hapCount;
	
	public HapFreqPair[] getHapFreq(Window w)
	{
		return hapFreq.get(w);
	}
	
	public byte[][] getHaplotypesArray(){
		return haplotypes;
	}
	
	/**
	 * @param path- full path to a .hap file
	 * which contains two haplotype (phased genotype) columns for each individual
	 * @throws IOException 
	 * @throws InterruptedException 
	 */
	public HaplotypesData(String path, int chrOffset, int[] blockBorders) throws IOException, InterruptedException {
		BlockReader blockReader = new BlockReader(path, chrOffset);
		haplotypes = blockReader.readBlock(blockBorders[0], blockBorders[1]);
	}
	
	/**
	 * 
	 * @param windows- all windows subsets to look through on the haplotype data
	 * 
	 * NOTE- this implementation limits window size to 16 because each haplotype is represented bitwise
	 * 
	 * @return mapping of 
	 */
	public Map<Window,HapFreqPair[]> getWindowHaplotypesFrequency(Window[] windows)
	{
		HashMap<Window,HapFreqPair[]> map = new HashMap<Window,HapFreqPair[]>();
		hapCount = new HashMap<Window,Map<Short,Integer>>();
		ArrayList<HapFreqPair> singleWindowMap;
		HashMap<Short,Integer> windowsCounters;
		
		short tempHapAsShort;
		Integer tempCounter;
		
		final int minOccurencesForFrequency = ConfigInfo.minOccurencesForFrequency;
		
		final int arrSize = (int) Math.pow(2, ConfigInfo.winSize) / 2; 
		for(int wid=0;wid<windows.length;wid++)
		{
			Window window = windows[wid];
			byte[] tempHap = new byte[window.size()];
			singleWindowMap = new ArrayList<HapFreqPair>(arrSize);
			windowsCounters = new HashMap<Short,Integer>();
			
			for(int indiv = 0;indiv<haplotypes.length;indiv++)
			{
				for(int markerIndex=0; markerIndex < window.size(); markerIndex++)
				{
					tempHap[markerIndex]=haplotypes[indiv][window.getMarker(markerIndex)];
				}
				tempHapAsShort = byteArrayToSingleShort(tempHap);
				tempCounter = windowsCounters.get(tempHapAsShort);
				if(tempCounter == null)
					windowsCounters.put(tempHapAsShort,0);
				windowsCounters.put(tempHapAsShort,windowsCounters.get(tempHapAsShort)+1);
			}
			//now windowsCounters holds how many times haplotype appeared
			hapCount.put(window, windowsCounters);
			
			//compute frequencies
			int totalHapsCount = haplotypes.length;
			for(Entry<Short, Integer> hapEntry : windowsCounters.entrySet()) {
				int hapCount = hapEntry.getValue();
				if(hapCount>minOccurencesForFrequency)
					singleWindowMap.add(new HapFreqPair(hapEntry.getKey(), hapCount/(double)totalHapsCount, hapCount, totalHapsCount)); 
			}
			map.put(window,singleWindowMap.toArray(new HapFreqPair[singleWindowMap.size()]));
		}
		
		hapFreq = map;
		return map;
	}
	
	public static short byteArrayToSingleShort(byte[] b)
	{
		assert(b.length!=Short.SIZE);
		short ret = 0;
		short temp;
		for(int i=0;i<b.length;i++)
        {
			if(b[i]==0) continue;
			temp=(short) (b[i] << i);
			ret+=temp;
        }
		return ret;
	}
	
	public static byte[] shortToByteArray(short a,int arrLength)
	{
		assert(arrLength!=Short.SIZE);
		byte[] ret=new byte[arrLength];
		int help;
		for(int i=0;i<arrLength;i++)
		{
			help = 1 << i;
			ret[i]=(byte) ((help&a)==0 ? 0 : 1);
		}
		return ret;
	}

	public static void main(String[] args) {
		short s = 0b0001110010110011;
		System.out.println(s);
		
		byte[] bArr = shortToByteArray(s, 16);
		System.out.println(Arrays.toString(bArr));
		
		short s2 = byteArrayToSingleShort(bArr);
		System.out.println(s2);
	
		byte[] bArr2 = shortToByteArray(s2, 16);
		System.out.println(Arrays.toString(bArr2));
	}
}