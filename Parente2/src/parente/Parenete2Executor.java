package parente;
import helper.IndexPair;
import helper.SettingsGetter;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Parenete2Executor {
	final static String logMessage = "[{0}]: {1}";

	public static void main(String [ ] args) {
		final Logger logger = Logger.getLogger(IBDBlockDetector.class.getName());
		
		if(args.length != 5) {
			System.out.println("Incorect # of arguments. There should be 5 arguments.");
			System.out.println("correct format is: <genotypeFilePath> <haplotypeFilePath> <snpFilePath> <outputResultPath> <configPath>");
			return;
		}
		
		String genotypeFilePath = args[0];
		String haplotypeFilePath = args[1];
		String snpInfoFilePath = args[2];
		String outputPath = args[3];
		String configPath = args[4];
		
		SettingsGetter settingsGetter=new SettingsGetter(configPath);
		try {
			settingsGetter.fillSettings();
		} catch (Exception e1) {
			e1.printStackTrace();
			System.exit(-1);
		}
		
		int[] idc = null;
		SnpInfoReader snpInfoReader = new SnpInfoReader();
		try {
			snpInfoReader.fillSnpInfo(snpInfoFilePath);
			idc = new IdConverter(genotypeFilePath).getIndexToId();
		} catch (NumberFormatException | IOException e) {
			e.printStackTrace();
			System.exit(-1);
		} 
		
		GHHCache ghhCache = new GHHCache(ConfigInfo.winSize, ConfigInfo.winSize);
		ConcurrentLinkedQueue<Map<IndexPair,List<int[]>>> resultQueue = new ConcurrentLinkedQueue<Map<IndexPair,List<int[]>>>();
		
		Chromosome[] chrArr = new Chromosome[23];
		String fileSeparator = System.getProperty("file.separator");
		String chrTempDirPath = outputPath.substring(0, outputPath.lastIndexOf(fileSeparator));
		
		for(int i=1; i<=22; i++){
			try {
				chrArr[i] = new Chromosome(i, snpInfoReader, resultQueue, chrTempDirPath + fileSeparator + "chr"+i+".txt");
			} catch (IOException e) {
				e.printStackTrace();
				System.exit(-1);
			}
		}
		
		AtomicBoolean isDoneDetectingBlocks = new AtomicBoolean(false);
		
		OutputWriter outputWriter = null;
		try {
			outputWriter = new OutputWriter(outputPath, idc);
		} catch (IOException e1) {
			e1.printStackTrace();
			System.exit(-1);
		}		
		Thread OutputWriterThread = new Thread(new OutputWriterRunner(outputWriter, resultQueue, isDoneDetectingBlocks));
		
		logger.log(Level.FINEST, logMessage, new Object[]{Thread.currentThread().getName(),"Started OutputWriter Thread..."});
		OutputWriterThread.start();
		
		ConcurrentLinkedQueue<Block> blockQueue = new ConcurrentLinkedQueue<>();
		
		AtomicBoolean isDoneReadingBlocks = new AtomicBoolean(false);
		AtomicInteger readChrNum = new AtomicInteger(0);
		
		for(int i=0; i<ConfigInfo.blockReaderThreadsNum; i++){
			Thread ibdBlockReaderRunner = new Thread(new IBDBlockReaderRunner(blockQueue, chrArr, genotypeFilePath, snpInfoReader, isDoneReadingBlocks, readChrNum, i+1));
			logger.log(Level.INFO, logMessage, new Object[]{Thread.currentThread().getName(),"Started ibdBlockReaderRunner Thread..."});
			ibdBlockReaderRunner.start();
		}
		
		ExecutorService execService = Executors.newWorkStealingPool();
		Block currBlock = null;
		
		AtomicInteger atomicInt = new AtomicInteger(0);
		
		while(!isDoneReadingBlocks.get()) {
			while(atomicInt.get() <= 2*ConfigInfo.blockDetectorThreadsNum && (currBlock = blockQueue.poll())!=null) {
				int numOfJobs = atomicInt.getAndIncrement();
				//logger.log(Level.INFO, logMessage, new Object[]{Thread.currentThread().getName(),": Registered job #"+numOfJobs+"..."});
				execService.execute(new IBDBlockDetectorRunner(haplotypeFilePath, chrArr[currBlock.getChromosomeNum()], currBlock, resultQueue, atomicInt, ghhCache));
			}
			
			try {
				Thread.sleep((long) (3*1000));
			} catch (InterruptedException e) {
				e.printStackTrace();
				System.exit(-1);
			}
		}
		
		while((currBlock = blockQueue.poll())!=null) {
			int numOfJobs = atomicInt.getAndIncrement();
			//logger.log(Level.INFO, logMessage, new Object[]{Thread.currentThread().getName(),": Registered job #"+numOfJobs+"..."});
			execService.execute(new IBDBlockDetectorRunner(haplotypeFilePath, chrArr[currBlock.getChromosomeNum()], currBlock, resultQueue, atomicInt, ghhCache));
		}
				
		try {
			execService.shutdown();
			execService.awaitTermination(Long.MAX_VALUE, TimeUnit.DAYS);
			isDoneDetectingBlocks.set(true);
		} catch (InterruptedException e) {
			e.printStackTrace();
			System.exit(-1);
		}
		
		try {
			OutputWriterThread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
			System.exit(-1);
		}
		
		System.out.println("Totally Done!!! =]");
	}
}
