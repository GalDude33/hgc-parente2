package parente;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.SortedSet;
import java.util.TreeSet;


public class TrainData {

	private double keepFratcion;
	private int numOfPairs;
	private HaplotypesData hapData;
	private Window[] wins;
	
	// getWins() return array of window ids (as integers) in region
	private GHHCache ghhCache;

	/**
	 * Train- filter windows with high variance that can fault the IBD
	 * detection. -use haplotypes of individuals from the dataset to simulate
	 * genotypes of related and unrelated pairs -compute window variance for all
	 * windows and for all pairs -take only windows with low variance
	 * 
	 * required info for training: LRT2 type, pseudocound and hist bins, path to
	 * .hap file (phased data), window size, number of pairs to simulate,
	 * windows to filter from, maximum of haplotypes per window,fraction of
	 * windows to keep
	 * 
	 * example usage: TrainData("LRT2Hist",new double[]{0.01,30},
	 * "data/ddd.hap",5,100,...)
	 */
	public TrainData(HaplotypesData hapData, int numOfPairs, Window[] wins, double keepFraction, GHHCache ghhCache) {
		//System.out.println("Initializing TrainData..");
		this.numOfPairs = numOfPairs;
		this.wins = wins;
		this.hapData = hapData;
		this.keepFratcion = keepFraction;
		this.ghhCache = ghhCache;
	}
	
	private boolean intArrayContains(int[] arr, int size, int val) {
		for(int i=0; i<size; i++)
			if(arr[i]==val)
				return true;
		
		return false;
	}
	
	private void simulateTrainingPairs(byte[][] simulatedGenosRelated, byte[][] simulatedGenosUnRelated) {
		byte[][] haplotypes = hapData.getHaplotypesArray();
		int[] unrelatedHaps = new int[4];
		int[] relatedHaps = new int[3];
		int numHaps = haplotypes.length;
		int numOfMarkers = haplotypes[0].length;
		Random randomGenerator = new Random();
		int ind1, ind2;
		
		for (int p = 0; p < this.numOfPairs; p++) {
			for (int i = 0; i < 4; i++) {// choose haplotypes for the 2 pairs
				
				// generate unique
				int hapIndex = randomGenerator.nextInt(numHaps);
				if(i>=2) {
					while(intArrayContains(unrelatedHaps, 2, hapIndex)){
						hapIndex = randomGenerator.nextInt(numHaps);
					}
				}
				unrelatedHaps[i] = hapIndex;
				
				if (i < 3){
					relatedHaps[i] = randomGenerator.nextInt(numHaps);
				}
			}

			ind1 = 2 * p;
			ind2 = ind1 + 1;
			
			simulatedGenosRelated[ind1] = new byte[numOfMarkers];
			simulatedGenosRelated[ind2] = new byte[numOfMarkers];
			simulatedGenosUnRelated[ind1] = new byte[numOfMarkers];
			simulatedGenosUnRelated[ind2] = new byte[numOfMarkers];
			
			for (int m = 0; m < numOfMarkers; m++) {
				simulatedGenosUnRelated[ind1][m] = (byte) (haplotypes[unrelatedHaps[0]][m] + haplotypes[unrelatedHaps[1]][m]);
				simulatedGenosUnRelated[ind2][m] = (byte) (haplotypes[unrelatedHaps[2]][m] + haplotypes[unrelatedHaps[3]][m]);
				simulatedGenosRelated[ind1][m] = (byte) (haplotypes[relatedHaps[0]][m] + haplotypes[relatedHaps[1]][m]);
				simulatedGenosRelated[ind2][m] = (byte) (haplotypes[relatedHaps[1]][m] + haplotypes[relatedHaps[2]][m]);
			}
		}
	}

	public Window[] run() {
		Window[] winsAfterFilter;
		double[][] winLrt1ScoresRelated = new double[wins.length][];// according to window index in wins array
		double[][] winLrt1ScoresUnRelated = new double[wins.length][];// according to window index in wins array
		
		List<WindowRegion> windowRegions = new ArrayList<WindowRegion>();
		createWindowRegions(wins, windowRegions);
		
		List<Integer> winsToKeep = new ArrayList<Integer>();
		WindowStatistics[] winStatisticArr = new WindowStatistics[wins.length];
		
		byte[][] simulatedGenosRelated;
		byte[][] simulatedGenosUnRelated;/* * 1st index: individual,
													* 2nd index: genotype at marker */
		simulatedGenosRelated = new byte[this.numOfPairs * 2][];
		simulatedGenosUnRelated = new byte[this.numOfPairs * 2][];
		simulateTrainingPairs(simulatedGenosRelated,simulatedGenosUnRelated);
		
		// Compute window-level LRT1
		computeScoresForSimulation(simulatedGenosRelated, winLrt1ScoresRelated);
		computeScoresForSimulation(simulatedGenosUnRelated, winLrt1ScoresUnRelated);
		
		for (int w = 0; w < wins.length; w++) {
			winStatisticArr[w] = new WindowStatistics(winLrt1ScoresUnRelated[w], winLrt1ScoresRelated[w]);
		}
		
		int currWinId;
		double score;
		Map<Integer, Double> winScores;
		int winEnd;
		List<Window> winsInRegion;
		List<Integer> winsIdsInRegion;
		
		for (WindowRegion wr : windowRegions) {
			winScores = new HashMap<Integer, Double>();
			winsInRegion = wr.getWins();
			winsIdsInRegion = wr.getWinsIds();

			// for each window in current region
			for (int i = 0; i < winsInRegion.size(); i++) {
				currWinId = winsIdsInRegion.get(i);
				score = winStatisticArr[currWinId].getMedean_r()-winStatisticArr[currWinId].getMedean_u();
				winScores.put(currWinId, score);
			}
			
			// NOTE- winScores is only for current region
			SortedSet<Map.Entry<Integer,Double>> sortedEntries = new TreeSet<Entry<Integer, Double>>(new Comparator<Map.Entry<Integer,Double>>() {
						@Override
						public int compare(Entry<Integer, Double> o1, Entry<Integer, Double> o2) {
							// Ascending order - o2 compare to o1
							return o2.getValue().compareTo(o1.getValue());
						}
					});
			
			sortedEntries.addAll(winScores.entrySet());
			winEnd = (int) Math.ceil(keepFratcion * (double) winScores.size());
			
			// Cool way to cut at winEnd (limit), and extract values (mapToInt)
			//sortedEntries.stream().limit(winEnd).mapToInt(e -> e.getKey())
			// add each item to the list
			//.forEachOrdered(i -> winsToKeep.add(i));
			
			Iterator<Entry<Integer, Double>> itr = sortedEntries.iterator();
			int i=0;
			
			while(i<winEnd && itr.hasNext()) {
				Entry<Integer, Double> currEntry = itr.next();
				winsToKeep.add(currEntry.getKey());
				++i;
			}
		}
		
		int i = 0;
		winsToKeep.sort(null);// natural comperator
		winsAfterFilter = new Window[winsToKeep.size()];
		WindowStatistics winStatistics;
		
		for (int winToKeepId : winsToKeep) {// winToKeep is original id from wins
			winStatistics = winStatisticArr[winToKeepId];
			winsAfterFilter[i] = wins[winToKeepId];
			winsAfterFilter[i].setWinStatistics(winStatistics);
			i++;
		}
		
		return winsAfterFilter;
	}

	private void computeScoresForSimulation(byte[][] simulationGenArr, double[][] winScoresArr) {
		int currIndex = 0; 
		
		for (int ind = 0; ind < simulationGenArr.length; ind += 2) {
			ModelCopyWithMul winLRT1ApproxModel = new ModelCopyWithMul(simulationGenArr[ind], simulationGenArr[ind + 1], hapData, ghhCache);
			currIndex = (int)(ind/2);
			
			for (int w = 0; w < wins.length; w++) {
				if(winScoresArr[w]==null) { 
					winScoresArr[w] = new double[simulationGenArr.length/2];
				}
				double winLRT1 = winLRT1ApproxModel.Calculate(wins[w]);
				winScoresArr[w][currIndex] = winLRT1;
			}
		}
	}

	class WindowComparator implements Comparator<Window> {
		public int compare(Window a, Window b) {
			int leftMostDiff = a.getLeftMostMarker() - b.getLeftMostMarker();
			return leftMostDiff!=0 ? leftMostDiff : a.getRightMostMarker() - b.getRightMostMarker();
		}
	}

	private final float max_window_region_size_cm = 0.10f;

	private void createWindowRegions(Window[] wins, List<WindowRegion> windowRegions) {
		/*
		 * We assume wins are sorted by start, but not stop. Therefore, to make
		 * each non-overlapping region, we must accumulate all windows that may
		 * *possibly* fit inside the region (ie grab all of them that have a
		 * start within the target region) then we will only keep ones that
		 * actually fit inside
		 */
		//System.out.println("Creating window regions..");
		// Create a set of window indices that are ordered
		Window currWin;
		List<Window> ws = new ArrayList<Window>();
		List<Window> temp;
		Map<Window, Integer> winToIdMap = new HashMap<Window, Integer>();
		Iterator<Window> iter;

		for (int w = 0; w < wins.length; w++) {
			currWin = wins[w];
			ws.add(currWin);
			winToIdMap.put(currWin, w);
		}

		ws.sort(new WindowComparator());

		while (ws.size() > 0) {
			// First gather all windows with a start inside our target size
			double startCM = indexToCM(ws.get(0).getLeftMostMarker());
			double maxEndCM = startCM + max_window_region_size_cm;

			WindowRegion newRegion = new WindowRegion(startCM);
			windowRegions.add(newRegion);

			iter = ws.iterator();
			temp = new ArrayList<Window>();
			currWin = iter.next();
			temp.add(currWin);

			while (iter.hasNext() && indexToCM(currWin.getLeftMostMarker()) <= maxEndCM) {
				currWin = iter.next();
				temp.add(currWin);
			}

			// Check which of the possible windows fit into the region
			iter = temp.iterator();
			while (iter.hasNext()) {
				currWin = iter.next();
				if (currWin == ws.get(0) || indexToCM(currWin.getRightMostMarker()) <= maxEndCM) {
					newRegion.addWin(currWin);
					newRegion.addWinId(winToIdMap.get(currWin));
				}
			}
			// Compute the stop cm based on wins assigned to the new region Also remove the windows from the window set
			newRegion.setStopCM(newRegion.getStartCM());
			for (Window w : newRegion.getWins()) {
				newRegion.setStopCM(Math.max(newRegion.getStopCM(), indexToCM(w.getRightMostMarker())));
				ws.remove(w); //TODO check
			}
		}
	}

	private double indexToCM(int rightMostMarker) {
		return rightMostMarker*(double)(1/ConfigInfo.genomePosVsCmRatio);
	}
}
