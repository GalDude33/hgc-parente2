package parente;
import java.util.ArrayList;
import java.util.List;


public class WindowRegion {

	private final List<Window> wins;
	private final List<Integer> winsIds;
	private final double startCM;
	private double stopCM;
	
	public WindowRegion(double startCM) {
		this.startCM = startCM;
		wins = new ArrayList<Window>();
		winsIds = new ArrayList<Integer>();
	}
	
	public List<Window> getWins() {
		return wins;
	}
	
	public void addWin(Window w)
	{
		wins.add(w);
	}
	
	public void addWinId(int wid)
	{
		winsIds.add(wid);
	}
	
	public List<Integer> getWinsIds()
	{
		return winsIds;
	}
	
	public double getStartCM() {
		return startCM;
	}
	
	public double getStopCM() {
		return stopCM;
	}
	
	public void setStopCM(double stopCM) {
		this.stopCM = stopCM;
	}
}
