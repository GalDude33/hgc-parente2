package parente;

import java.util.Queue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;

public class IBDBlockReaderRunner implements Runnable {

	private Queue<Block> blockQueue;
	private String genotypeFilePath;
	private SnpInfoReader snpInfoReader;
	private AtomicBoolean isDoneReadingBlocks;
	private AtomicInteger readChrNum;
	private int startChr;
	private Chromosome[] chrArr;
	public static final Logger logger = Logger.getLogger(IBDBlockReaderRunner.class.getName());

	public IBDBlockReaderRunner(Queue<Block> queue, Chromosome[] chrArr, String genotypeFilePath,
			SnpInfoReader snpInfoReader, AtomicBoolean isDoneReadingBlocks, AtomicInteger readChrNum, int startChr) {
		this.blockQueue = queue;
		this.chrArr = chrArr;
		this.genotypeFilePath = genotypeFilePath;
		this.snpInfoReader = snpInfoReader;
		this.isDoneReadingBlocks = isDoneReadingBlocks;
		this.readChrNum = readChrNum;
		this.startChr = startChr;
		// logger.setLevel(Level.OFF);
	}

	@Override
	public void run() {
		try {
			for (int chr = startChr; chr <= ConfigInfo.totalChrNum; chr += ConfigInfo.blockReaderThreadsNum) {
				// int chr = 21;
				System.out.println("started reading chromosome: " + chr);
				BlockGenerator blockGenerator = new BlockGenerator(genotypeFilePath, snpInfoReader, chr);

				int blockNum = 0;
				while (blockGenerator.hasNext()) {
					Block block = blockGenerator.next();
					// chromosome.addBlock(block);
					blockQueue.add(block);
					logger.log(Level.INFO, ConfigInfo.logMessage, new Object[] { Thread.currentThread().getName(),
							"Read Block " + (++blockNum) });

					while (blockQueue.size() >= /* 2* */ConfigInfo.blockDetectorThreadsNum / 4.0) {
						try {
							Thread.sleep((long) (7 * 1000));
						} catch (InterruptedException e) {
							e.printStackTrace();
							throw e;
						}
					}
				}

				chrArr[chr].setBlocksNum(blockNum);

				if (readChrNum.incrementAndGet() == ConfigInfo.totalChrNum) {
					isDoneReadingBlocks.set(true);
				}

				logger.log(Level.INFO, ConfigInfo.logMessage, new Object[] { Thread.currentThread().getName(),
						"Done reading chromosome " + chr + "..." });
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(-1);
		}
	}
}