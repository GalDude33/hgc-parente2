package parente;

public class Block {
	
	private final byte[][] genotypes;
	//private Snp[] snpsInfo;
	private final int chromosomeOffset;
	private final int[] blockBorders;
	private final int chromosomeNum;

	public Block(byte[][] genotypes,/* Snp[] snpInfo,*/ int chromosomeOffset, int[] blockBorders, int chrNum){
		this.genotypes = genotypes;
		//this.snpsInfo = snpInfo;
		this.chromosomeOffset = chromosomeOffset;
		this.blockBorders = blockBorders;
		this.chromosomeNum = chrNum;
	}
	
	public int getChromosomeNum() {
		return chromosomeNum;
	}
	
	public byte[][] getGenotypes() {
		return genotypes;
	}
	
	public int size() {
		return genotypes[0].length;
	}
	
	/*public Snp[] getSnpInfo() {
		return snpsInfo;
	}*/
	
	public int getChromosomeOffset() {
		return chromosomeOffset;
	}
	
	public int[] getBlockBorders() {
		return blockBorders;
	}
}
