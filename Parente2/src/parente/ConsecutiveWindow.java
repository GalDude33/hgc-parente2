package parente;

public class ConsecutiveWindow extends AbsWindow {
	private final int _start;
	private final int _end;
	
	public ConsecutiveWindow(int start, int end) {
		this._start = start;
		this._end = end;
	}

	@Override
	public int size() {
		return _end-_start+1;
	}

	@Override
	public int getMarker(int i) {
		return _start+i;
	}

	public int getRightMostMarker(){
		return _end;
	}
	
	public int getLeftMostMarker(){
		return _start;
	}

	/*@Override
	public double getMedianScore() {
		// TODO Auto-generated method stub
		return 0;
	}*/
}
