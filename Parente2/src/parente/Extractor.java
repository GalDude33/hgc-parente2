package parente;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class Extractor {
	List<String> fileList;
    private static String INPUT_ZIP_FILE="" ;
    private static String OUTPUT_FOLDER = "";
    
	public static String extract() throws IOException, InterruptedException {
		System.out.println("Extracting scripts..");
		INPUT_ZIP_FILE = GetExecutionPath();
		OUTPUT_FOLDER = INPUT_ZIP_FILE.substring(0, INPUT_ZIP_FILE.lastIndexOf("/"));
		System.out.println("out: "+OUTPUT_FOLDER);
		new File(OUTPUT_FOLDER+File.separator +"scripts").mkdirs();
		Thread.sleep(2000);
		unZipIt(INPUT_ZIP_FILE, OUTPUT_FOLDER);
		return OUTPUT_FOLDER+File.separator +"scripts";
	}
	
	public static void unZipIt(String zipFile, String outputFolder){
		 
	     byte[] buffer = new byte[1024];
	 
	     try{
	 
	    	//create output directory is not exists
	    	File folder = new File(OUTPUT_FOLDER);
	    	if(!folder.exists()){
	    		folder.mkdir();
	    	}
	 
	    	//get the zip file content
	    	ZipInputStream zis = 
	    		new ZipInputStream(new FileInputStream(zipFile));
	    	//get the zipped file list entry
	    	ZipEntry ze = zis.getNextEntry();
	    	while(ze!=null&&!ze.getName().contains("scripts"))
   			 ze = zis.getNextEntry();
	    	ze = zis.getNextEntry();
	    	while(ze!=null&&ze.getName().contains("scripts")){
	    		
	    	   String fileName = ze.getName();
	    	   if(ze.isDirectory())
	    		   continue;
	           File newFile = new File(outputFolder + File.separator + fileName);
	 
	            FileOutputStream fos = new FileOutputStream(newFile,true);             
	 
	            int len;
	            while ((len = zis.read(buffer)) > 0) {
	       		fos.write(buffer, 0, len);
	            }
	 
	            fos.close();   
	            ze = zis.getNextEntry();
	    	}
	 
	        zis.closeEntry();
	    	zis.close();
	 
	    	System.out.println("Done");
	 
	    }catch(IOException ex){
	       ex.printStackTrace(); 
	       System.exit(-1);
	    }
	   }    
	private static String GetExecutionPath(){
	    String fullPath = Extractor.class.getProtectionDomain().getCodeSource().getLocation().getPath();
	    fullPath = fullPath.replaceAll("%20"," "); // Surely need to do this here
	    return fullPath;
	}
}
