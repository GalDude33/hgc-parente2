package parente;

public abstract class AbsWindow implements Window {
	
	private WindowStatistics winStatistics;
	//private int id;
	
	@Override
	public Genotype Apply(byte[] genotype) {
		return new Genotype(genotype, this);
	}
	
	@Override
	public WindowStatistics getWinStatistics() {
		return winStatistics;
	}

	@Override
	public void setWinStatistics(WindowStatistics winStatistics) {
		this.winStatistics = winStatistics;
	}
	
	/*@Override
	public double getMedianScore() {
		return (winStatistics.getMean_r()*2.52);
	}
	
	@Override
	public void setId(int id){
		this.id = id;
	}
	
	@Override
	public int getId(){
		return id;
	}*/
}
