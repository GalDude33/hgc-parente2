package parente;

import helper.HapFreqPair;
import java.util.ArrayList;


public class ModelCopyWithMul {
	private byte[] _g1;
	private byte[] _g2;
	private HaplotypesData haplotypesData;
	private final float geno_error = (float) ConfigInfo.genotypeError;
	private GHHCache ghhCache;

	public ModelCopyWithMul(byte[] g1, byte[] g2, HaplotypesData hapData, GHHCache ghhCache) {
		this._g1 = g1;
		this._g2 = g2;
		this.haplotypesData = hapData;
		this.ghhCache = ghhCache;
	}

	//log(exp(a) + exp(b)) = a + log(1 + exp(b-a))
	/*double LogAdd(final double num1, final double num2) {
		if (num1 > num2)
			return Math.log(Math.exp(num2) + Math.exp(num1));//Math.log1p(Math.exp(num2 - num1)) + num1;
		else if (num1 < num2)
			return Math.log(Math.exp(num2) + Math.exp(num1));//Math.log1p(Math.exp(num1 - num2)) + num2;

		return Math.log(2) + num1; // 2 * num1
	}*/

	double ProbGGivenHH(Genotype geno, short h1, short h2, float error_rate) {
		//GHHTriplet ghhTriplet = new GHHTriplet(geno.asShort,h1,h2);
		double prob = ghhCache.getGHHProb(geno.asShort, h1, h2);
		
		if(prob<0){
			prob = 1.0;
			final int genoSize = geno.size();
			
			for (int i = 0; i < genoSize; ++i) {
				if (((h1 >> i) & 1) + ((h2 >> i) & 1) == geno.get(i)) {
					prob *= (1.0 - error_rate);
				} else {
					prob *= (error_rate / 2);
				}
			}
			
			ghhCache.addGHHProb(geno.asShort, h1, h2, prob);
		} 
		
		return prob;
	}

	public double CalcIBDModel(ArrayList<HapFreqPair> hapProbMap, Genotype g1, Genotype g2) {
		double prob_rel = 0;

		// P(g1, g2 | IBD) = \sum_{h1,h2,h3} P(g1 | h1,h2) * P(g2 | h2,h3) *
		// P(h1) * P(h2) * P(h3)
		final int hapProbMapSize = hapProbMap.size();
		HapFreqPair tmpHapFreqPair;
		for (int i1=0;i1<hapProbMapSize;i1++) {
			tmpHapFreqPair = hapProbMap.get(i1);
			final short h1 = tmpHapFreqPair.h;
			final double prob_h1 = tmpHapFreqPair.f;

			for (int i2=0;i2<hapProbMapSize;i2++) {
				tmpHapFreqPair = hapProbMap.get(i2);
				final short h2 = tmpHapFreqPair.h;
				final double prob_h2 = tmpHapFreqPair.f;

				// P(g1 | h1,h2)
				final double prob_g1_given_hh = ProbGGivenHH(g1, h1, h2, geno_error);

				for (int i3=0;i3<hapProbMapSize;i3++) {
					tmpHapFreqPair = hapProbMap.get(i3);
					final short h3 = tmpHapFreqPair.h;
					final double prob_h3 = tmpHapFreqPair.f;

					// P(g2 | h2,h3)
					final double prob_g2_given_hh = ProbGGivenHH(g2, h2, h3, geno_error);

					final double prob_ghhh = prob_g1_given_hh * prob_g2_given_hh * prob_h1 * prob_h2 * prob_h3;

					prob_rel += prob_ghhh;
				}
			}
		}

		return Math.log(prob_rel);
	}

	private double CalcIBDModel2(final HapFreqPair[] hapProbMap, final Genotype g1, final Genotype g2) {
		double prob_rel = 0;
		
		final int hapProbMapSize = hapProbMap.length;
		final float geno_error = this.geno_error;
		HapFreqPair tmpHapFreqPair;
	    // P(g1, g2 | IBD) = \sum_{h2}{P(h2) * \sum_{h1}{P(g1 | h1,h2) * P(h1)} * \sum_{h3}{P(g2 | h2,h3)*P(h3)}
		for (int i2=0;i2<hapProbMapSize;i2++) {
			tmpHapFreqPair = hapProbMap[i2];
			final short h2 = tmpHapFreqPair.h;
			final double prob_h2 = tmpHapFreqPair.f;
	   
			double sum1 = 0;
		    double sum2 = 0;
	     
		    for (int i1=0;i1<hapProbMapSize;i1++) {
				tmpHapFreqPair = hapProbMap[i1];
				final short h1 = tmpHapFreqPair.h;
				final double prob_h1 = tmpHapFreqPair.f;
				double prob_g1_given_hh = ProbGGivenHH(g1,h1,h2,geno_error);
				sum1 += prob_h1 * prob_g1_given_hh;
		    }
	    
		    for (int i3=0;i3<hapProbMapSize;i3++) {
				tmpHapFreqPair = hapProbMap[i3];
				final short h3 = tmpHapFreqPair.h;
				final double prob_h3 = tmpHapFreqPair.f;
				double prob_g2_given_hh = ProbGGivenHH(g2,h2,h3,geno_error);
				sum2 += prob_h3 * prob_g2_given_hh;
		    }

		    double prob_ghhh = prob_h2 * sum1 * sum2;
		    prob_rel += prob_ghhh;
	    }
		
		return Math.log(prob_rel);
	}
	
	public double CalcNonIBDModel(final HapFreqPair[] hapProbMap, Genotype g1, Genotype g2) {
		double prob_unr1 = 0; // p(g1) = \sum_{h,h} p(g1|hh) p(h)
											// p(h)
		double prob_unr2 = 0; // p(g2) = \sum_{h,h} p(g1|hh) p(h)
											// p(h)

		// P(g1, g2 | IBD) = \sum_{h1,h2} {P(g1 | h1,h2) + P(g2 | h1,h2)}
		final int hapMapSize = hapProbMap.length;
		for (int i1 = 0; i1 < hapMapSize; ++i1) {
			final HapFreqPair sdPair1 = hapProbMap[i1];
			final short h1 = sdPair1.h;
			final double prob_h1 = sdPair1.f;

			// Do upper right part of the matrix, we will multiply by 2
			for (int i2 = i1; i2 < hapMapSize; ++i2) {
				final HapFreqPair sdPair2 = hapProbMap[i2];
				final short h2 = sdPair2.h;
				final double prob_h2 = sdPair2.f;

				// P(g1 | h1,h2)
				final double prob_g1_given_hh = ProbGGivenHH(g1, h1, h2, geno_error);

				// P(g2 | h1,h2)
				final double prob_g2_given_hh = ProbGGivenHH(g2, h1, h2, geno_error);

				final double p_hh = prob_h1 * prob_h2;
				double prob_ghhh1 = prob_g1_given_hh * p_hh; // p(g1|hh)*p(h)p(h)
				double prob_ghhh2 = prob_g2_given_hh * p_hh; // p(g2|hh)*p(h)p(h)

				// If we are not on the diagonal, multiply by 2
				if (i1 != i2) {
					prob_ghhh1 *= 2;
					prob_ghhh2 *= 2;
				}

				// Add to p(g1) and p(g2)
				prob_unr1 += prob_ghhh1;
				prob_unr2 += prob_ghhh2;
			}
		}

		final double prob_unr = prob_unr1 * prob_unr2; // p(g1 g2 | hhhh)
																// = p(g1|hh) *
																// p(g2|hh)

		return Math.log(prob_unr);
	}

	public double Calculate(Window win) {
		final Genotype g1 = win.Apply(this._g1);
		final Genotype g2 = win.Apply(this._g2);

		final HapFreqPair[] hapProbMap = hapProbsForWindow(win);

		// return log(P_I / P_I_NOT) == log(P_I) - log(P_I_NOT)
		return CalcIBDModel2(hapProbMap, g1, g2) - CalcNonIBDModel(hapProbMap, g1, g2);
	}

	private HapFreqPair[] hapProbsForWindow(Window win) {
		return haplotypesData.getHapFreq(win);
	}
}
