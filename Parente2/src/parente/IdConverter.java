package parente;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;


public class IdConverter {
	private int[] indexToID;
	
	public IdConverter(String path_to_geno) throws IOException {
		File genoFile = new File(path_to_geno);
		BufferedReader br = new BufferedReader(new FileReader(genoFile));
		String line = br.readLine();
		String[] ids = line.split("\\t");
		indexToID = new int[ids.length];
		String id;
		for (int i = 0; i<ids.length;i++) {
			id = ids[i];
			if(id.contains("#"))
				id=id.substring(1);
			indexToID[i] = Integer.parseInt(id);
		}
		
		br.close();
	}
	public int[] getIndexToId(){
		return indexToID;
	}
}
