package parente;

public class ConfigInfo {
	public static int genomePosVsCmRatio = 1000000;
	public static int blockSize = 8*genomePosVsCmRatio;
	
	public static int winSize = 5;
	public static int subsetSize = 10;//10;
	public static int regionSize = 15;
	public static int augmentedWindowsNumInRegion = 3;
	public static int minBlockSize = 40;
	
	public static double genotypeError = 0.01;
	
	public static int blockReaderThreadsNum = 1;//2
	public static double blockDetectorThreadsNum = 8;
	public static String logMessage = "[{0}]: {1}";
	
	public static int totalChrNum = 22;//22;
	public static int minIbdLength = 200;

	public static int numOfPairs = 5000;
	public static double keepFraction = 0.9;
	public static int minOccurencesForFrequency = 1;
	
	public static double gammaThreshold = 0.79;
	public static int logWriteGap = 20;	
	
	public static int personsNum;
	
	//public static int chr;
}
