package parente;

public interface Window {
	public Genotype Apply(byte[] genotype);
	public int size();
	public int getMarker(int i);
	public int getRightMostMarker();
	public int getLeftMostMarker();
	public WindowStatistics getWinStatistics();
	public void setWinStatistics(WindowStatistics winStatistics);
	//public double getMedianScore();
	//public void setId(int id);
	//public int getId();
}
