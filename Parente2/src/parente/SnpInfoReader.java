package parente;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SnpInfoReader {

	//private String filePath;
	private Map<Integer, List<Snp>> chromosomeSnpsMap;
	private int[] chromosomeOffsetMap;
	private MapUtils<Integer,Snp> mapUtils = new MapUtils<Integer,Snp>();
	private long numOfTotalSnps;
	
	public SnpInfoReader(/*String filePath*/) {
		//this.filePath = filePath;
		this.numOfTotalSnps = 0;
		this.chromosomeOffsetMap = new int[23];
		this.chromosomeSnpsMap = new HashMap<Integer, List<Snp>>();
	}
	
	public void fillSnpInfo(String filePath) throws NumberFormatException, IOException{
		File snpFile = new File(filePath);
		BufferedReader br = new BufferedReader(new FileReader(snpFile));
		String line = br.readLine();
		int prevChromosome = 0;
		int currChromosomeCount = 0;

		while ((line=br.readLine()) != null)
		{
	 		String[] snpInfo = line.split("\\t");
	 		//int id = Integer.parseInt(snpInfo[0].replaceAll("\\D+",""));
	 		int chromosome = Integer.parseInt(snpInfo[1].replaceAll("\\D+",""));
	 		int genomePosition = Integer.parseInt(snpInfo[2]);
	 		mapUtils.addEntryToMap(chromosomeSnpsMap, chromosome, new Snp(/*id, chromosome,*/ genomePosition));
	 		
	 		if(prevChromosome != chromosome){
	 			chromosomeOffsetMap[chromosome] = currChromosomeCount;
	 			prevChromosome = chromosome;
	 		}
	 		
	 		currChromosomeCount++;
	 		numOfTotalSnps++;
		}
		
		br.close();
	}
	
	public List<Snp> getChromosomeSnps(int chromosome){
		return chromosomeSnpsMap.get(chromosome);
	}
	
	public int getChromosomeOffset(int chromosome){
		return chromosomeOffsetMap[chromosome];
	}
	
	public int getNearestSnpIndexAtGivenPosition(int chromosome, int genomePos){
		final List<Snp> snpList = chromosomeSnpsMap.get(chromosome);
		int snpsNum = snpList.size();
		
		if(genomePos>snpList.get(snpsNum-1).getGenomePosition()){
			return snpsNum-1;
		}
		
		if(genomePos<snpList.get(0).getGenomePosition()){
			return 0;
		}
		
		Comparator<Snp> snpComparator = new Comparator<Snp>(){
			@Override
			public int compare(Snp currSnp, Snp targetSnp) {
				int currSnpFirstGenomePos = currSnp.getGenomePosition();
				int currFollowingSnpGenomePos = snpList.get(snpList.indexOf(currSnp)+1).getGenomePosition();
				int targetGenomePos = targetSnp.getGenomePosition();
				
				if(currSnpFirstGenomePos<targetGenomePos && currFollowingSnpGenomePos<targetGenomePos){
					return -1;
				}else if(currSnpFirstGenomePos>targetGenomePos && currFollowingSnpGenomePos>targetGenomePos){
					return 1;
				} else {
					return 0;
				}
			}
		};
		
		return Collections.binarySearch(snpList, new Snp(/*-1, chromosome, */genomePos), snpComparator);
	}

	public long getColumnsNum() {
		return numOfTotalSnps;
	}
	
	public int[] getChromosomeOffsetMap()
	{
		return chromosomeOffsetMap;
	}
}