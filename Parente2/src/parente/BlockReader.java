package parente;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;


public class BlockReader {

	private final String fileToReadPath;
	private final int chromosomeOffset;
	private int rowsNum;
	private int endlineLength;
	
	public BlockReader(String fileToReadPath, SnpInfoReader snpInfoReader, int chromosome) throws IOException {
		this.fileToReadPath = fileToReadPath;
		this.chromosomeOffset = snpInfoReader.getChromosomeOffset(chromosome);
		fileProperties();
		ConfigInfo.personsNum = rowsNum;
	}
	
	public BlockReader(String fileToReadPath, int chromosomeOffset) throws IOException {
		this.fileToReadPath = fileToReadPath;
		this.chromosomeOffset = chromosomeOffset;
		fileProperties();
	}
	
	private void fileProperties() throws IOException{
		try(FileReader fr = new FileReader(fileToReadPath)) {
			try(BufferedReader br = new BufferedReader(fr)) {
				this.rowsNum = br.readLine().split("\\s+").length; //changed from '\\t' to '\\s+'
				
				char c;
				do {
					c = (char) br.read();
				} while(c!='\r' && c!='\n');
				endlineLength = (((char) br.read()) == '\n') ? 2 : 1;
			}
		} catch (IOException e) {
			this.rowsNum = 0;
			throw e;
		}
	}
	
	public byte[][] readBlock(int startBlockIndex, int endBlockIndex) throws IOException{
		byte[][] dataMatrix = null;
		int columnsNum = endBlockIndex-startBlockIndex+1;
		
		try(FileReader fr = new FileReader(fileToReadPath)) {
			try(BufferedReader br = new BufferedReader(fr)) {
				dataMatrix = new byte[rowsNum][columnsNum];
				
				// skip first line
				int rowCharsLength = br.readLine().split("\\s+").length*2+(endlineLength-1);
				// skip to startBlockIndex line
				br.skip(((long)chromosomeOffset + (long)startBlockIndex) * (long)rowCharsLength);
							
				String line;
				
				int columsCounter = 0;
				int totalLines = endBlockIndex - startBlockIndex + 1;
				while (columsCounter<totalLines && (line = br.readLine()) != null && (line.length()>0))
				{
					String[] chrValues = line.split("\\s+");
			 		
					int currValIndex = 0;
					for(byte[] currPerson : dataMatrix) {
						currPerson[columsCounter] = Byte.parseByte(chrValues[currValIndex++]);
					}
					++columsCounter;
				}
			} catch (IOException e) {
				e.printStackTrace();
				throw e;
			}
		} catch (IOException e1) {
			e1.printStackTrace();
			throw e1;
		}
		
		return dataMatrix;
	}
}
