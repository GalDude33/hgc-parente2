package parente;
import helper.IndexPair;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Map;


public class OutputWriter {
	
	private FileWriter fw;
	private int[] indexToID;
	
	public OutputWriter(String outputPath, int[] indexToID) throws IOException{
		this.indexToID = indexToID;
		try {
			this.fw = new FileWriter(outputPath);
			fw.append("name1\tname2\tchr_start\tstart\tchr_end\tend\n");
		} catch (IOException e) {
			e.printStackTrace();
			throw e;
		}
	}
	
	public void writeOutput(Map<IndexPair, List<int[]>> currBlockResMap) throws IOException{
		for(Map.Entry<IndexPair, List<int[]>> entry : currBlockResMap.entrySet()) {
			int p1 = entry.getKey().first;
			int p2 = entry.getKey().second;
			int chr1 = 0;
			int chr2 = 0;
			for(int[] ibdBorders : entry.getValue()) {
				int startGenomeLocation = ibdBorders[0];
				int endGenomeLocation = ibdBorders[1];
				chr1 = chr2 = entry.getKey().chrNum;
				fw.append(indexToID[p1]+"\t"+indexToID[p2]+"\tchr"+chr1+"\t"+startGenomeLocation+"\tchr"+chr2+"\t"+endGenomeLocation+"\n");
			}
		}
	}
	
	public void close() throws IOException {
		fw.close();
	}
}
