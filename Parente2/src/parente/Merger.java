package parente;

import helper.IndexPair;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class Merger {

	/*
	 * private Snp[] snps;
	 * 
	 * public Merger(Snp[] snps) { this.snps = snps; }
	 */

	public void merge(Map<IndexPair, List<int[]>> ibdPairs) {
		for (List<int[]> twoIndsIBDs : ibdPairs.values()) {
			merge(twoIndsIBDs);
		}
	}

	private void merge(List<int[]> ibdResult) {
		if (ibdResult.size() <= 1)
			return;

		ibdResult.sort(new Comparator<int[]>() {
			@Override
			public int compare(int[] o1, int[] o2) {
				int leftDiff = o1[0] - o2[0];
				return leftDiff != 0 ? leftDiff : o1[1] - o2[1];
			}
		});

		{
			Iterator<int[]> itr = ibdResult.iterator();
			
			if (itr.hasNext()) {
				int[] mergePair = itr.next();
				
				while (itr.hasNext()) {
					int[] currPair = itr.next();

					// mergePair = 41 100
					// currPair = 41 200

					// [ { ] }
					if (currPair[0] - 1 <= mergePair[1] && mergePair[1] <= currPair[1]) {
						mergePair[1] = currPair[1];
						itr.remove();
					} else
					// [ { } ]
					if (currPair[1] <= mergePair[1]) {
						itr.remove();
					} else {
						mergePair = currPair;
					}
				}
			}
		}

		/*
		 * // removing ibd below threshold length Iterator<int[]> itr =
		 * ibdResult.iterator(); while(itr.hasNext()) { int[] mergePair1 =
		 * itr.next();
		 * 
		 * // convert to real ibd length mergePair1[0] =
		 * getLocationFromSnpIndex(mergePair1[0]); mergePair1[1] =
		 * getLocationFromSnpIndex(mergePair1[1]);
		 * 
		 * // remove if below threshold
		 * if(Math.abs(mergePair1[1]-mergePair1[0])<=ConfigInfo.minIbdLength) {
		 * itr.remove(); } }
		 */
	}

	public static void main(String[] args) {
		Map<IndexPair, List<int[]>> ibdPairs = new HashMap<IndexPair, List<int[]>>();
		IndexPair pair = new IndexPair(0, 1, 0);
		List<int[]> ibds = new ArrayList<int[]>();

		// []{}
		ibds.add(new int[] { 0, 1 });
		ibds.add(new int[] { 1, 2 }); // need to return 0-2
		// []{}
		ibds.add(new int[] { 20, 21 });
		ibds.add(new int[] { 22, 23 });
		ibds.add(new int[] { 24, 230 }); // need to return 20-23
		// [ { ] }
		ibds.add(new int[] { 4, 6 });
		ibds.add(new int[] { 5, 7 }); // need to return 4-7
		// [ { } ]
		ibds.add(new int[] { 10, 15 });
		ibds.add(new int[] { 12, 14 }); // need to return 10-15

		ibdPairs.put(pair, ibds);
		Merger merger = new Merger();
		merger.merge(ibdPairs);

		for (List<int[]> ibdFinalIndexs : ibdPairs.values()) {
			for (int[] indexs : ibdFinalIndexs) {
				System.out.println(indexs[0] + " " + indexs[1]);
			}
		}
	}
}
