package parente;
import helper.IndexPair;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicBoolean;


	public class OutputWriterRunner implements Runnable {
		
		private OutputWriter outputWriter;
		private Queue<Map<IndexPair, List<int[]>>> resQueue;
		private AtomicBoolean isDoneDetecting;
		
		public OutputWriterRunner(OutputWriter outputWriter, ConcurrentLinkedQueue<Map<IndexPair, List<int[]>>> resultQueue, AtomicBoolean isDoneDetecting) {
			this.outputWriter = outputWriter;
			this.resQueue = resultQueue;
			this.isDoneDetecting = isDoneDetecting;
		}

		@Override
		public void run() {
			try {
				Map<IndexPair,List<int[]>> currBlockResMap;
				
				while(!isDoneDetecting.get()) {
					while((currBlockResMap = resQueue.poll())!=null) {
						outputWriter.writeOutput(currBlockResMap);
					}
				
					Thread.sleep((long) (13*1000));
				}
				
				while((currBlockResMap = resQueue.poll())!=null) {
					outputWriter.writeOutput(currBlockResMap);
				}
				
				outputWriter.close();
			} catch (IOException | InterruptedException e) {
				e.printStackTrace();
				System.exit(-1);
			}
		}
	}
