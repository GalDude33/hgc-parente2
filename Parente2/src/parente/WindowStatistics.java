package parente;

public class WindowStatistics {

	private double sd_r;//standard deviation
	//private double sd_u;
	private double mean_r;//mean deviation
	//private double mean_u;
	//private double max_u;
	//private double max_r;
	//private double min_r;
	//private double min_u;
	private double medean_u;
	private double medean_r;
	//Statistics stat_u;
	//Statistics stat_r;
	
	public WindowStatistics(double[] lrt1UnrelatedScores,double[] lrt1RelatedScores) {
		Statistics stat_u = new Statistics(lrt1UnrelatedScores);
		Statistics stat_r = new Statistics(lrt1RelatedScores);
		
		// TODO: delete
		this.medean_u = stat_u.median();
		this.medean_r = stat_r.median();
		//this.mean_u = stat_u.getMean();
		this.mean_r = stat_r.getMean();
		this.sd_r = stat_r.getStdDev();
		//this.sd_u = stat_u.getStdDev();
		//this.max_r = stat_r.getMax();
		//this.max_u = stat_u.getMax();
		//this.min_r = stat_r.getMin();
		//this.min_u = stat_u.getMin();
	}

	public double getSd_r() {
		return sd_r;
	}

	/*public double getSd_u() {
		return sd_u;
	}*/
	
	public double getMean_r() {
		return mean_r;
	}
	
	/*public double getMean_u() {
		return mean_u;
	}*/
	
	/*public double getMax_r() {
		return max_r;
	}
	
	public double getMax_u() {
		return max_u;
	}*/
	
	/*public double getMin_u() {
		return min_u;
	}
	
	public double getMin_r() {
		return min_r;
	}*/

	public double getMedean_u() {
		return medean_u;
	}

	public double getMedean_r() {
		return medean_r;
	}
}
