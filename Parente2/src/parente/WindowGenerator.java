package parente;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;


public class WindowGenerator {

	private final int winSize = ConfigInfo.winSize;
	private final int regionSize = ConfigInfo.regionSize;
	private final int augmentedWindowsNumInRegion = ConfigInfo.augmentedWindowsNumInRegion;
	
	private List<Window> generateConsecutiveWindows(Block block){
		List<Window> windows = new ArrayList<Window>();
		int blockSize = block.size();
		
		for(int i=0; i<blockSize-1; i=i+winSize){
			if(i+winSize-1<=blockSize-1)
				windows.add(new ConsecutiveWindow(i, i+winSize-1));
			else
				windows.add(new ConsecutiveWindow(blockSize-winSize, blockSize-1));
		}
		
		return windows;
	}
	
	private boolean intArrayContains(int[] arr, int size, int val) {
		for(int i=0; i<size; i++)
			if(arr[i]==val)
				return true;
		
		return false;
	}
	
	private List<Window> generateNonConsecutiveWindows(Block block){
		List<Window> windows = new ArrayList<Window>();
		int blockSize = block.size();
		
		Random rand = new Random();
		for(int i=0; i<blockSize; i=i+regionSize) {
			// make min for winsize in case we don't have enough left
			int startIndex = i+regionSize-1>blockSize-1 ? Math.max(blockSize - regionSize,0) : i;
			
			for(int j=0; j<augmentedWindowsNumInRegion; j++){
				int[] markers = new int[winSize/*Math.min(winSize, blockSize-i)*/];
				
				if((blockSize-startIndex) < regionSize) {
					// too little range for random - generate deterministically
					ArrayList<Integer> list = new ArrayList<Integer>();
			        for (int r=startIndex; r<blockSize-1; r++) {
			            list.add(r);
			        }
			        Collections.shuffle(list);
					for(int m=0; m<winSize; m++) {
						markers[m]= list.get(i); //startIndex + (int)(m * ((blockSize-startIndex)/(double)winSize));
					}
				} else {
					for(int m=0; m<winSize; m++) {
						int tmpMarker;
						do {
							tmpMarker = startIndex + (int)(rand.nextInt(Math.min(regionSize, blockSize-startIndex)));
						} while(intArrayContains(markers,m,tmpMarker));
						markers[m]= tmpMarker;
					}
				}
				
				windows.add(new NonConsecutiveWindows(markers));
			}
		}
		
		return windows;
	}
	
	public List<Window> generateWindows(Block block){
		List<Window> windows = new ArrayList<Window>();
		windows.addAll(generateConsecutiveWindows(block));
		windows.addAll(generateNonConsecutiveWindows(block));
		return windows;
	}
}
