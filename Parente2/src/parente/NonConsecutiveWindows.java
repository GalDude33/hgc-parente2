package parente;
import java.util.Arrays;


public class NonConsecutiveWindows extends AbsWindow {
	private int[] _markers;
	
	public NonConsecutiveWindows(int[] markers) {
		Arrays.sort(markers);
		this._markers = markers;
	}

	@Override
	public int size() {
		return _markers.length;
	}

	@Override
	public int getMarker(int i) {
		return _markers[i];
	}
	
	public int getRightMostMarker(){
		return _markers[_markers.length-1];
	}
	
	public int getLeftMostMarker(){
		return _markers[0];
	}
}
