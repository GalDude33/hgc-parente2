package parente;

public class Genotype {
	private final byte[] _genotype;
	private final Window _win;
	public final short asShort;
	
	public Genotype(byte[] genotype, Window win) {
		this._genotype = genotype;
		this._win = win;
		this.asShort = computeGShortValue();
	}
	
	public int size() {
		return _win.size();
	}
	
	public byte get(int i) {
		return _genotype[_win.getMarker(i)];
	}
	
	public byte[] getGenotype() {
		byte[] res = new byte[_win.size()];
		
		for(int i=0; i<_win.size(); i++){
			res[i] = get(i);
		}
		
		return res;
	}
	
	private short computeGShortValue(){	
		short res = 0;
		
		final int arrLength = size();
		for(int i=0; i<arrLength; i++){
			res += get(i)*Math.pow(3, i);
		}
	
		return res;
	}
	/*private static short getByteArrIntValue(byte[] arr, int arrSize, int base){
		short res = 0;
		
		for(int i=0; i<arrSize; i++){
			res += arr[i]*Math.pow(base, i);
		}
	
		return res;
	}*/
}
