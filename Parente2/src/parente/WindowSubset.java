package parente;
import java.util.List;


public class WindowSubset {


	private final List<Window> windows;
	private final Block block;
	
	public WindowSubset(Block block, List<Window> windows/*, double threshold*/){
		this.block = block;
		this.windows = windows;
	}
	
	public Block getBlock(){
		return block;
	}
	
	public List<Window> GetWindows(){
		return windows;
	}
	
	private int _max = -1;
	public int GetRightMostMarker() {
		if(_max >= 0) return _max;
		
		int _max = 0;
		
		for(Window window : windows){
			int currWindowRightMostMarker = window.getRightMostMarker();
			if(currWindowRightMostMarker > _max){
				_max = currWindowRightMostMarker;
			}
		}
		
		return _max;
	}
	
	private int _min = -1;
	public int GetLeftMostMarker() {
		if(_min>=0) return _min;
		
		_min = Integer.MAX_VALUE;
		
		for(Window window : windows){
			int currWindowLeftMostMarker = window.getLeftMostMarker();
			if(currWindowLeftMostMarker < _min){
				_min = currWindowLeftMostMarker;
			}
		}
		
		return _min;
	}
}
