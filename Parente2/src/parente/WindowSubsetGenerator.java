package parente;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;


public class WindowSubsetGenerator {

	private int subsetSize = ConfigInfo.subsetSize;
	
	private double getWindowSubsetScore(List<Window> windows) { 
		/*int winDataSize = ConfigInfo.numOfPairs;
		double[] lrt1UnrelatedScores = new double[winDataSize*windows.size()];
		double[] lrt1RelatedScores = new double[winDataSize*windows.size()];
		
		int i=0;
		for(Window win : windows){
			WindowStatistics winS = win.getWinStatistics();
			System.arraycopy(winS.lrt1UnrelatedScores,0, lrt1UnrelatedScores, i, winDataSize);
			System.arraycopy(winS.lrt1RelatedScores,0, lrt1RelatedScores, i, winDataSize);
			i+=winDataSize;
		}
		
		return new WindowStatistics(lrt1UnrelatedScores,lrt1RelatedScores);*/
		/*int winDataSize = ConfigInfo.numOfPairs;
		
		double[] relatedData = new double[winDataSize*windows.size()];
		double[] unrelatedData = new double[winDataSize*windows.size()];
		int i=0;
		
		// for all unrelated pairs
		int[] unrel_count=new int[winDataSize];
		for(int p=0; p<winDataSize; p++) {
			for(Window win : windows) {
				if(win.getWinStatistics().stat_u.data[p] > win.getWinStatistics().getMedean_r()) {
					++unrel_count[p];
				}
			}
		}
		
		int unrel_total = 0;
		for(int per : unrel_count)
			if(per>=windows.size()/2.5)
				unrel_total++;
				
		
		// for all unrelated pairs
		int[] rel_count=new int[winDataSize];
		for(int p=0; p<winDataSize; p++) {
			for(Window win : windows) {
				if(win.getWinStatistics().stat_r.data[p] > win.getWinStatistics().getMedean_r()) {
					++rel_count[p];
				}
			}
		}
		
		int rel_total = 0;
		for(int per : rel_count)
			if(per>=windows.size()/2.5)
				rel_total++;
		
		for(Window win : windows) {
			double[] tmpSrc;
			
			tmpSrc = win.getWinStatistics().stat_r.data;
			System.arraycopy(tmpSrc, 0, relatedData, i*winDataSize, winDataSize);
			
			tmpSrc = win.getWinStatistics().stat_u.data;
			System.arraycopy(tmpSrc, 0, unrelatedData, i*winDataSize, winDataSize);
			
			++i;
		}
		
		WindowStatistics wsStat = new WindowStatistics(unrelatedData,relatedData);*/
		
		return 0;//wsStat.getMean_r();
	}
	
	private void sortWindowsByLeftMostMarker(List<Window> windows) {
		Comparator<Window> leftMostWinComp = new Comparator<Window>() {
			
			@Override
			public int compare(Window w1, Window w2) {
				return w1.getLeftMostMarker() - w2.getLeftMostMarker();
			}
		};
		
		windows.sort(leftMostWinComp);
	}
	
	private void sortWindowsByRightMostMarker(List<Window> windows) {
		Comparator<Window> rightMostWinComp = new Comparator<Window>() {
			
			@Override
			public int compare(Window w1, Window w2) {
				return w1.getRightMostMarker() - w2.getRightMostMarker();
			}
		};
		
		windows.sort(rightMostWinComp);
	}
	
	public List<WindowSubset> generateWindowSubsets(Block block, List<Window> windows){
		List<WindowSubset> windowSubsets = new ArrayList<WindowSubset>();
		int windowsNum = windows.size();
		
		sortWindowsByLeftMostMarker(windows);
		/*int j=0;
		for(Window win : windows){
			win.setId(j++);
		}*/
		
		int wsStepSize = Math.round(subsetSize/2);
		for(int i=0; i<windowsNum; i+=wsStepSize){
			List<Window> currWindowSubsetWins = windows.subList(i, Math.min(windowsNum, i+subsetSize));
			windowSubsets.add(new WindowSubset(block, currWindowSubsetWins /*,getWindowSubsetScore(currWindowSubsetWins)*/));
		}
		
		/*sortWindowsByRightMostMarker(windows);
		
		for(int i=0; i<windowsNum; i=i+subsetSize){
			List<Window> currWindowSubsetWins = windows.subList(i, Math.min(windowsNum, i+subsetSize));
			windowSubsets.add(new WindowSubset(block, currWindowSubsetWins, getWindowSubsetScore(currWindowSubsetWins)));
		}*/
		
		return windowSubsets;
	}
}
