package unused;

public class HaplotypePair {
	byte[] firstHap;
	byte[] secHap;
	
	public HaplotypePair(byte[] firstHap, byte[] secHap) {
		this.firstHap = firstHap;
		this.secHap = secHap;
	}
	
	public byte[] getFirst() {
		return firstHap;
	}

	public byte[] getSecond() {
		return secHap;
	}
}
