package unused;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import parente.ConfigInfo;
import parente.Genotype;

public class HaplotypeEnumarator implements Iterable<HaplotypePair>,
		Iterator<HaplotypePair> {

	//private byte[] genotype;
	private byte[] currHaplotype1;
	private byte[] currHaplotype2;
	private List<Integer> genotypeOneLocations;
	private BinaryCounterEnumarator binaryCounter;
	
	//private static List<Integer>[] genotypeOneLocationsBank = new ArrayList[(int) Math.pow(3, ConfigInfo.winSize)];
	
	public HaplotypeEnumarator(Genotype genotype) {
		byte[] genotypeArr = genotype.getGenotype();
		this.currHaplotype1 = new byte[genotypeArr.length];
		this.currHaplotype2 = new byte[genotypeArr.length];
		
		/*int genoAsInt = byte3ArrayToInt(genotypeArr);
		if((genotypeOneLocations = genotypeOneLocationsBank[genoAsInt])==null) {
			// One locations not in bank :\ generate!
			genotypeOneLocations = new ArrayList<Integer>(genotypeArr.length);
			setGenotypeOneLocations(genotypeArr);
			genotypeOneLocationsBank[genoAsInt]=genotypeOneLocations;
		}*/
		genotypeOneLocations = new ArrayList<Integer>(genotypeArr.length);
		setGenotypeOneLocations(genotypeArr);
		this.binaryCounter = new BinaryCounterEnumarator(genotypeOneLocations.size());
	}

	@Override
	public Iterator<HaplotypePair> iterator() {
		return this;
	}

	@Override
	public boolean hasNext() {
		return binaryCounter.hasNext();
	}

	@Override
	public HaplotypePair next() {
		byte[] arr1 = binaryCounter.next();
		int i=0;
		
		for(int loc : genotypeOneLocations){
			currHaplotype1[loc]=arr1[i];
			currHaplotype2[loc]=(byte) (1-arr1[i]);
			i++;
		}
		
		return new HaplotypePair(currHaplotype1, currHaplotype2);
	}
	
	private void setGenotypeOneLocations(byte[] genotype) {
		for(int i=0; i<genotype.length; i++) {
			switch(genotype[i]) {
				case 0:
					currHaplotype1[i]=currHaplotype2[i]=0;
					break;
				case 1:
					genotypeOneLocations.add(i);
					break;					
				case 2:
					currHaplotype1[i]=currHaplotype2[i]=1;
					break;
			}
		}
	}
	
	public void reset() {
		binaryCounter.reset();
	}
	
	public static int byte3ArrayToInt(byte[] b)
	{
		assert(b.length<=ConfigInfo.winSize);
		short ret = 0;
		for(int i=0;i<b.length;i++)
        {
			ret+=b[i]*Math.pow(3,i);
        }
		return ret;
	}
}

