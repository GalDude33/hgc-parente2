package unused;
import java.util.Arrays;
import java.util.Iterator;


public class BinaryCounterEnumarator implements Iterable<byte[]>,Iterator<byte[]> {

	private byte[] arr;
	private boolean firstTime = true;
	private int count;
	
	public BinaryCounterEnumarator(int arrSize){
		this.arr = new byte[arrSize];
		this.count = (int) Math.pow(2, arrSize);
	}
	
	@Override
	public Iterator<byte[]> iterator() {
		return this;
	}
	
	@Override
	public boolean hasNext() {
		return count>0;
	}
	
	@Override
	public byte[] next() {
		if(firstTime){
			firstTime = false;
		} else {
			arr[arr.length-1]++;  // Increment the ones digit
		    int i = arr.length-1;
		    
		    while (i>=1 && arr[i] == 2) {  // Do carries
		    	arr[i] = 0;
		    	arr[i-1]++;
		        --i;
		    }
		}
		
	    --count;
	    return arr;
	}

	public void reset() {
		Arrays.fill(arr, (byte)0);
		this.count = (int) Math.pow(2, arr.length);
		firstTime = true;
	}
}
