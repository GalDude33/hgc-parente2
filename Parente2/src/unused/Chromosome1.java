package unused;
import helper.IndexPair;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.atomic.AtomicInteger;


public class Chromosome1 {

	public int num;
	private SnpInfoReader snpInfo;
	private AtomicInteger numOfRes = new AtomicInteger(0);
	//private List<Block> blocks = new ArrayList<Block>();
	private Map<IndexPair,List<int[]>> ibdPairs = new HashMap<IndexPair,List<int[]>>();
	private int blockNum;
	private Merger merger = new Merger(); 
	private MapUtils<IndexPair,int[]> mapUtils = new MapUtils<IndexPair,int[]>();
	private Queue<Map<IndexPair, List<int[]>>> resQueue;
	
	public Chromosome1(int num, SnpInfoReader snpInfo, Queue<Map<IndexPair, List<int[]>>> resQueue){
		this.num = num;
		this.snpInfo = snpInfo;
		this.resQueue = resQueue;
	}
	
	public void setBlocksNum(int blockNum) {
		this.blockNum = blockNum;
	}
	
	public void addBlockRes(Map<IndexPair,List<int[]>> blockRes) {
		mapUtils.addEntriesToMapFromMap(ibdPairs, blockRes);
		int resNum = numOfRes.incrementAndGet();
		
		if(resNum==blockNum){
			//call merge on all chrom
			merger.merge(ibdPairs);
			convertToGenomeLocations(ibdPairs);
			resQueue.add(ibdPairs);
		}
	}
	
	private void convertToGenomeLocations(Map<IndexPair, List<int[]>> ibdPairs) {
		List<Snp> chrSnps = snpInfo.getChromosomeSnps(num);
		
		for(List<int[]> currList : ibdPairs.values()) {
			for(int[] pair : currList) {
				pair[0] = chrSnps.get(pair[0]).getGenomePosition();
				pair[1] = chrSnps.get(pair[1]).getGenomePosition();
			}
		}
	}
}
