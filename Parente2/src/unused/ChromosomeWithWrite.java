package unused;
import helper.IndexPair;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.atomic.AtomicInteger;

import temp.IBDResultForChr;


public class ChromosomeWithWrite {

	public int num;
	private SnpInfoReader snpInfo;
	private AtomicInteger numOfRes = new AtomicInteger(0);
	//private List<Block> blocks = new ArrayList<Block>();
	private List<int[]> ibdPairs = new ArrayList<int[]>();
	private int blockNum;
	private Merger merger = new Merger(); 
	private MapUtils<IndexPair,int[]> mapUtils = new MapUtils<IndexPair,int[]>();
	private Queue<Map<IndexPair, List<int[]>>> resQueue;
	private FileWriter chrOutputFile;
	private String chrOutputPath;
	
	public ChromosomeWithWrite(int num, SnpInfoReader snpInfo, Queue<Map<IndexPair, List<int[]>>> resQueue, String chrOutputPath){
		this.num = num;
		this.snpInfo = snpInfo;
		this.resQueue = resQueue;
		this.chrOutputPath = chrOutputPath;
		try {
			this.chrOutputFile = new FileWriter(chrOutputPath);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void setBlocksNum(int blockNum) {
		this.blockNum = blockNum;
	}
	
	public void addBlockRes(Map<IndexPair,List<int[]>> blockRes) {
		try {
			writeOutput(blockRes);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		//mapUtils.addEntriesToMapFromMap(ibdPairs, blockRes);
		int resNum = numOfRes.incrementAndGet();
		
		if(resNum==blockNum){
			try {
				chrOutputFile.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			for(int i=0;i<ConfigInfo.personsNum;i++) {
				for(int j=i+1;j<ConfigInfo.personsNum;j++) {
					IndexPair indexPair = new IndexPair(i, j, num);
					List<int[]> ibdPairs = readChrOutput(i, j);
					merger.merge(ibdPairs);
					convertToGenomeLocations(ibdPairs);
					Map<IndexPair,List<int[]>> subResMap = new HashMap<IndexPair,List<int[]>>();
					subResMap.put(indexPair, ibdPairs);
					resQueue.add(subResMap);
				}
			}
			
			new File(chrOutputPath).delete();
			//call merge on all chrom
			//merger.merge(ibdPairs);
			//convertToGenomeLocations(ibdPairs);
			//resQueue.add(ibdPairs);
		}
	}
	
	private void convertToGenomeLocations(List<int[]> ibdPairs) {
		List<Snp> chrSnps = snpInfo.getChromosomeSnps(num);
		
		for(int[] pair : ibdPairs) {
			pair[0] = chrSnps.get(pair[0]).getGenomePosition();
			pair[1] = chrSnps.get(pair[1]).getGenomePosition();
		}
	}
	
	public synchronized void writeOutput(Map<IndexPair, List<int[]>> currBlockResMap) throws IOException{
		for(Map.Entry<IndexPair, List<int[]>> entry : currBlockResMap.entrySet()) {
			int p1 = entry.getKey().first;
			int p2 = entry.getKey().second;
			
			for(int[] ibdBorders : entry.getValue()) {
				int startLoc = ibdBorders[0];
				int endLoc = ibdBorders[1];
				chrOutputFile.append(p1+"\t"+p2+"\t"+startLoc+"\t"+endLoc+"\n");
			}
		}
	}
	
	public List<int[]> readChrOutput(int i, int j) {
		List<int[]> ibdPairs = new ArrayList<int[]>();
		
		try(FileReader fr = new FileReader(chrOutputPath)) {
			try(BufferedReader br = new BufferedReader(fr)) {
				String line;
				
				while ((line = br.readLine()) != null && (line.length()>0))
				{
					String[] values = line.split("\\s+");
					int p1 = Integer.parseInt(values[0]);
					int p2 = Integer.parseInt(values[1]);
					
			 		if((p1==i && p2==j) || (p1==j && p2==i)){
			 			int ibd1Pos = Integer.parseInt(values[2]);
			 			int ibd2Pos = Integer.parseInt(values[3]);
			 			ibdPairs.add(new int[]{ibd1Pos, ibd2Pos});
			 		}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return ibdPairs;
	}
}
